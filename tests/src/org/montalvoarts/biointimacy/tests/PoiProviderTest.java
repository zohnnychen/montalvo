package org.montalvoarts.biointimacy.tests;

import org.montalvoarts.biointimacy.data.Constants;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiProvider;
import org.montalvoarts.biointimacy.data.PoiResContract;
import org.montalvoarts.biointimacy.sync.PoiService;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;
import android.text.TextUtils;
import android.util.Log;

/**
 * To run this test, you can type: adb shell am instrument -w \ -e class
 * <org.montalvo...PoiProviderTest \
 * org.montalvoarts.biointimacy.tests/android.test.InstrumentationTestRunner
 */
public class PoiProviderTest extends ProviderTestCase2<PoiProvider> {

	public static class PoiBean {
		public ContentValues val;

		public PoiBean(String guid, String title, int lat, int longitude,
				String thumbUri, String resUri) {
			val = new ContentValues();
			val.put(Col.GUID.col, guid);
			val.put(Col.TITLE.col, title);
			val.put(Col.LAT.col, lat);
			val.put(Col.LONG.col, longitude);
			val.put(Col.THUMB_URI.col, thumbUri);
			val.put(Col.BODY.col, resUri);
		}
	}

	public static PoiBean d1 = new PoiBean("abcd", "Oak tree", 100, 122,
			"content://foo/100", "content://foo/2343");
	public static PoiBean d2 = new PoiBean("abcd", "Oak tree2", 101, 122,
			"content://foo/100", "content://foo/2343");

	public static PoiBean createSample() {
		return new PoiBean("abcd", "Oak tree", 100, 122, "content://foo/100",
				"content://foo/2343");
	}

	private static final String TAG = "TEST";

	private MockContentResolver mResolver;

	// @formatter:on
	public PoiProviderTest() {
		super(PoiProvider.class, Constants.AUTHORITY);
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		mResolver = getMockContentResolver();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

		mResolver.delete(PoiContract.CONTENT_URI, null, null);
	}

	/**
	 * Test basic functionality with simple, known-good data.
	 */
	public void testPoiCRUD() {

		// Try simple query
		Cursor c = null;
		try {
			c = mResolver
					.query(PoiContract.CONTENT_URI, null, null, null, null);
			assertNotNull(c);
			// the cursor (and DB) should be empty
			assertFalse(c.moveToFirst());
			c.close();

			// Simple insert
			Uri uri = mResolver.insert(PoiContract.CONTENT_URI, d1.val);
			// Query it again
			c = mResolver
					.query(PoiContract.CONTENT_URI, null, null, null, null);
			assertEquals("expected a poi", c.getCount(), 1);
			c.close();

			// Query by URI
			Log.d(TAG, "looking for URI, " + uri);
			// Query by /serials/#
			c = mResolver.query(uri, PoiContract.Col.proj(), null, null, null);
			assertNotNull(c);
			assertTrue(c.moveToNext());
			assertEquals(c.getString(PoiContract.Col.GUID.ind), "abcd");
			assertEquals(c.getString(PoiContract.Col.TITLE.ind), "Oak tree");
			assertEquals(c.getInt(PoiContract.Col.LAT.ind), 100);
			assertEquals(c.getInt(PoiContract.Col.LONG.ind), 122);
			assertEquals(c.getString(PoiContract.Col.THUMB_URI.ind),
					"content://foo/100");
			assertEquals(c.getString(PoiContract.Col.BODY.ind),
					"content://foo/2343");
			c.close();

			// Update attribute
			ContentValues cv = new ContentValues();
			cv.put(PoiContract.Col.TITLE.col, "Oak tree new");
			cv.put(PoiContract.Col.LAT.col, 200);
			cv.put(PoiContract.Col.NOTE_USN.col, 101);
			assertEquals(mResolver.update(uri, cv, null, null), 1);

			// check value
			c = mResolver.query(uri, PoiContract.Col.proj(), null, null, null);
			assertNotNull(c);
			assertTrue(c.moveToNext());
			assertEquals(c.getString(PoiContract.Col.TITLE.ind), "Oak tree new");
			assertEquals(c.getInt(PoiContract.Col.LAT.ind), 200);
			assertEquals(c.getInt(PoiContract.Col.NOTE_USN.ind), 101);
			c.close();

			// check delete
			assertEquals(mResolver.delete(uri, null, null), 1);
			c = mResolver
					.query(PoiContract.CONTENT_URI, null, null, null, null);
			assertNotNull(c);
			assertEquals(c.getCount(), 0);
			c.close();

		} finally {
			if (c != null)
				c.close();
		}
	}

	public void testPoiFromCursor() {

		// Try simple query
		Cursor c = null;
		try {

			// Simple insert
			d1.val.put(PoiContract.Col.NOTE_USN.col, 102);
			d1.val.put(PoiContract.Col.THUMB_DWNLD_ID.col, 10);
			Uri uri = mResolver.insert(PoiContract.CONTENT_URI, d1.val);
			// Query it again
			c = mResolver
					.query(PoiContract.CONTENT_URI, null, null, null, null);
			assertEquals("expected a poi", c.getCount(), 1);
			c.close();

			// Query by URI
			Log.d(TAG, "looking for URI, " + uri);
			Poi poi = PoiHelper.SINGLETON.create(getMockContext(), uri);
			assertNotNull(poi);

			assertEquals(poi.getString(PoiContract.Col.GUID), "abcd");
			assertEquals(poi.getString(PoiContract.Col.TITLE), "Oak tree");
			assertEquals(poi.getDouble(PoiContract.Col.LAT), 100.0, 0);
			assertEquals(poi.getDouble(PoiContract.Col.LONG), 122.0, 0);
			assertEquals(102, poi.getInteger(PoiContract.Col.NOTE_USN));
			assertEquals(10, poi.getInteger(PoiContract.Col.THUMB_DWNLD_ID));
			assertEquals(poi.getString(PoiContract.Col.THUMB_URI),
					"content://foo/100");
			assertEquals(poi.getString(PoiContract.Col.BODY),
					"content://foo/2343");

			assertTrue(poi.isValid());
		} finally {
			if (c != null)
				c.close();
		}
	}

	@SuppressLint("NewApi")
	public void testUpdateMultiple() {
		mResolver.insert(PoiContract.CONTENT_URI, d1.val);
		mResolver.insert(PoiContract.CONTENT_URI, d2.val);

		Cursor c = mResolver.query(PoiContract.CONTENT_URI,
				PoiContract.Col.proj(), Col.LAT.col + "=?",
				new String[] { "100" }, null);
		assertTrue(c != null & c.getCount() == 1);
		c.close();

		c = mResolver.query(PoiContract.CONTENT_URI, PoiContract.Col.proj(),
				Col.THUMB_DWNLD_ID.col + "=?", new String[] { "100" }, null);
		assertTrue(c != null & c.getCount() == 0);

		// now update all
		ContentValues values = new ContentValues();
		values.put(Col.LAT.col, 100);
		int numUpdated = mResolver.update(PoiContract.CONTENT_URI, values,
				Col.GUID.col + "=?", new String[] { "abcd" });
		assertEquals(2, numUpdated);
		c.close();

		// now check that there's 2 with the same latitude
		c = mResolver.query(PoiContract.CONTENT_URI, PoiContract.Col.proj(),
				Col.LAT.col + "=?", new String[] { "100" }, null);
		assertTrue(c != null & c.getCount() == 2);

	}

	public void testGetType() {
		Uri uri3 = PoiHelper.SINGLETON.formUriFromNoteGuid("foobar");
		Uri[] uris = new Uri[] { PoiContract.CONTENT_URI,
				ContentUris.withAppendedId(PoiContract.CONTENT_URI, 100), uri3 };
		String suffix = getProvider().getItemType(
				PoiProvider.PATH_ITEM_ID_SUFFIX);
		Log.d(TAG, "got type for suffix " + suffix);
		for (Uri r : uris) {
			String type = getProvider().getType(r);

			Log.d(TAG, "for uri " + r + ", got mimetype " + type);
			assertNotNull(type);
		}
	}

	public void testResCRUD2() {
    	   // Simple insert
        Uri uri = mResolver.insert(PoiContract.CONTENT_URI, d1.val);
        ContentValues v1 = createRes();
        v1.put(PoiResContract.GUID, "guid100");
        v1.put(PoiResContract.DOWNLOAD_ID, 100);
        Uri resUri = mResolver.insert(PoiContract.RESOURCES.getUri(uri),  v1);
        assertNotNull(resUri);
        
        ContentValues v2 = createRes();
        v2.put(PoiResContract.GUID, "guid200");
        v2.put(PoiResContract.DOWNLOAD_ID, 200);
        resUri = mResolver.insert(PoiContract.RESOURCES.getUri(uri),  v2);
        assertNotNull(resUri);
        
        Cursor c = mResolver.query(PoiResContract.ALL_RESOURCES, PoiResContract.Col.proj(), null, null, null);
        assertEquals(2,c.getCount());
        c.close();
        
        c = mResolver.query(PoiResContract.ALL_RESOURCES, PoiResContract.Col.proj(), PoiResContract.DOWNLOAD_ID+ "=?", new String[] {"200"}, null);
        assertEquals(1,c.getCount());
        assertTrue(c.moveToNext());
        assertEquals("guid200", c.getString(PoiResContract.Col.GUID.ind));
        c.close();
	}                 

	public void testResCRUD() {
		// Simple insert
		Uri uri = mResolver.insert(PoiContract.CONTENT_URI, d1.val);
		ContentValues v1 = createRes();
		v1.put(PoiResContract.GUID, "guid100");
		mResolver.insert(PoiContract.RESOURCES.getUri(uri), v1);

		ContentValues v2 = createRes();
		Uri resUri = mResolver.insert(PoiContract.RESOURCES.getUri(uri), v2);
		assertNotNull(resUri);
		assertTrue(!TextUtils.isEmpty(resUri.toString()));


		Cursor c = mResolver.query(resUri, PoiResContract.Col.proj(), null,
				null, null);
		assertTrue(c != null && c.moveToNext());
		assertEquals(c.getString(PoiResContract.Col.HASH.ind), "hash");
		assertEquals(c.getString(PoiResContract.Col.GUID.ind), "fooguid");
		assertEquals(c.getInt(PoiResContract.Col.DOWNLOAD_ID.ind), 101);
		assertTrue(c.getInt(PoiResContract.Col.POI_ID.ind) > 0);
	
		c.close();

		// test update it
		ContentValues v3 = new ContentValues();
		v3.put(PoiResContract.GUID, "newguid");
		mResolver.update(resUri, v3, null, null);
		c = mResolver
				.query(resUri, PoiResContract.Col.proj(), null, null, null);
		assertTrue(c != null && c.moveToNext());
		assertEquals("newguid", c.getString(PoiResContract.Col.GUID.ind));
		c.close();

		// get a bunch of them
		Uri query = PoiContract.RESOURCES.getUri(uri);
		Log.d(TAG, "got query " + query);
		c = mResolver.query(query, PoiResContract.Col.proj(), null, null, null);
		assertNotNull(c);
		assertEquals(2, c.getCount());
		while (c.moveToNext()) {
			String poiguid = c.getString(PoiResContract.Col.GUID.ind);
			Log.d(TAG, "got guid " + poiguid);
			assertTrue(!TextUtils.isEmpty(poiguid));
		}
		c.close();

		// test delete
		assertTrue(mResolver.delete(uri, null, null) == 1);
		c = mResolver.query(query, null, null, null, null);
		assertNotNull(c);
		assertEquals(0, c.getCount());
	}
	
	public void testTypes() {
		// Simple insert
		Uri poiUri = mResolver.insert(PoiContract.CONTENT_URI, d1.val);
		ContentValues v1 = createRes();
		v1.put(PoiResContract.GUID, "guid100");
		Uri resUri = mResolver.insert(PoiContract.RESOURCES.getUri(poiUri), v1);
		
		String type = mResolver.getType(poiUri);
		assertEquals(PoiService.MIMETYPE_POI, type);
		type = mResolver.getType(resUri);
		assertEquals(PoiService.MIMETYPE_POIRES, type);
	}

	private ContentValues createRes() {
		ContentValues v = new ContentValues();
		v.put(PoiResContract.Col.GUID.col, "fooguid");
		v.put(PoiResContract.Col.HASH.col, "hash");
		v.put(PoiResContract.Col.DOWNLOAD_ID.col, 101);

		return v;
	}
}
