package org.montalvoarts.biointimacy.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.sync.EvernoteClient;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.test.AndroidTestCase;
import android.text.TextUtils;
import android.util.Log;

import com.evernote.edam.notestore.NoteStore.Client;
import com.evernote.edam.notestore.SyncChunk;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;

public class EvernoteClientTest extends AndroidTestCase {

	private Client mClient;
	private EvernoteClient mEvernote;
	private DownloadManager mDownloader;

	protected void setUp() throws Exception {
		super.setUp();
		Log.d(TAG,"Got context " + getContext().getClass());
		mEvernote = new EvernoteClient(mContext);
		mClient = mEvernote.connect();
		mDownloader = (DownloadManager) mContext
				.getSystemService(Context.DOWNLOAD_SERVICE);
		assertNotNull(mDownloader);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		mEvernote.reset();
	}

	public void testConnect() {
		EvernoteClient client = new EvernoteClient(mContext);
		Client noteClient = client.connect();
		assertNotNull(noteClient);
	}

	public void testCheckAndSetLastUsn() {
		// Before sync
		assertEquals(0, mEvernote.getLastUsn());
		assertTrue(mEvernote.checkAndSetLastUsn(mClient));
		// After first check and no syncs have yet happened, usn should be 0
		assertEquals(0, mEvernote.getLastUsn());
	}

	public void testGetSyncChunk() {
		long beforeLastSync = mEvernote.getLastSync();
		long beforeLastUsn = mEvernote.getLastUsn();
		assertEquals(0, beforeLastUsn);
		SyncChunk chunk = mEvernote.getSyncChunk(mClient);

		assertNotNull(chunk);
		int lastUsn = mEvernote.getLastUsn();
		long lastSync = mEvernote.getLastSync();
		assertNotSame("After successful sync,  lastUsn should change",
				beforeLastUsn, lastUsn);
		assertTrue(beforeLastSync < lastSync);

		// This change should have been persisted to pref
		EvernoteClient client2 = new EvernoteClient(mContext);
		assertEquals(lastUsn, client2.getLastUsn());
		assertEquals(lastSync, client2.getLastSync());
	}

	public static final String KNOWN_NOTE_GUID = "e669c090-d8b2-4324-9eae-56bd31c64af7";
	private static final String TAG = "TEST";

	public void testGetThumb() throws InterruptedException {
		mEvernote.checkAndSetLastUsn(mClient);
		long id = mEvernote.downloadNoteThumbnail(KNOWN_NOTE_GUID);
		assertTrue(id > 0);
		assertDownloaded(id);
	}
	
	public void testGetNote() throws MalformedURLException, IOException {
		Note note1 = getFirstNote(mEvernote, mClient);
		String noteGuid = note1.getGuid();

		Note note = mEvernote.getNote(mClient, noteGuid);
		String body = note.getContent();
		assertTrue(note.isSetUpdateSequenceNum());
		assertTrue(note.getUpdateSequenceNum() > 0);
		assertTrue(note.isSetGuid());
		assertTrue(note.isSetContent());
		// Get the content from server
		Log.d(TAG,"Got note content " + body);
		assertTrue(!TextUtils.isEmpty(body));
	}

	public void testGetRealThumb() throws InterruptedException {
		mEvernote.checkAndSetLastUsn(mClient);
		Note note = getFirstNote(mEvernote, mClient);
		long id = mEvernote.downloadNoteThumbnail(note.getGuid());
		assertTrue(id > 0);
		assertDownloaded(id);
	}

	public void testPoiHelper_create() {
		Note note = getFirstNote(mEvernote, mClient);
		String nGuide = note.getGuid();
		String nTitle = note.getTitle();
		Double nLat = note.getAttributes().getLatitude();
		
		Poi p = PoiHelper.SINGLETON.create(note);
		assertNotNull(p);
		assertTrue(p.isValid());
		assertEquals(nGuide, p.getString(Col.GUID));
		assertEquals(nTitle, p.getString(Col.TITLE));
		assertEquals(nLat, p.getDouble(Col.LAT));
	}
	
	public static Note getFirstNote(EvernoteClient evernote, Client client) {
		SyncChunk chunk = evernote.getSyncChunk(client);
		assertNotNull(chunk);
		assertTrue(chunk.getNotesSize() > 0);
		List<Note> notes = chunk.getNotes();
		assertNotNull(notes);
		assertTrue(notes.size() > 0);
		Note note1 = null;
		for (Note n : notes) {
			Log.d(TAG, "Got note " + n.getTitle() + ", noteGuid " + n.getGuid());
			List<Resource> ress = n.getResources();
			if (ress != null) {
				for (Resource res : ress) {
					Log.d(TAG, "Got res: " + res.getGuid());
				}
			}
			if (!n.isSetDeleted() && ress != null && ress.size() > 0) {
				note1 = n;
			}
		}
	
		assertTrue(note1.isSetGuid());
		assertTrue(note1.getResourcesSize() > 0);
		assertTrue(note1.getResources() != null
				&& note1.getResources().size() > 0);
		return note1;
	}

	private void assertDownloaded(long id) throws InterruptedException {
		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterById(id);

		int status = -1;
		for (int i = 0; i < 10; i++) {
			Thread.sleep(3000);
			Cursor c = mDownloader.query(query);
			if (c == null) {
				Log.d(TAG, "No cursor");
				break;
			}
			Log.d(TAG, "Finished waiting and cursor length: " + c.getCount());
			if (c.getCount() > 0 && c.moveToNext()) {
				int numBytes = c
						.getInt(c
								.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
				status = c.getInt(c
						.getColumnIndexOrThrow(DownloadManager.COLUMN_STATUS));
				int reason = c.getInt(c
						.getColumnIndexOrThrow(DownloadManager.COLUMN_REASON));

				Log.d(TAG, "Got numbytes " + numBytes + " and status " + status);
				if (status == DownloadManager.STATUS_SUCCESSFUL
						|| status == DownloadManager.STATUS_FAILED) {
					Log.d(TAG, "Download completed " + status + ", reason "
							+ reason);
					
					break;
				}
			}
		}
		assertTrue(status == DownloadManager.STATUS_SUCCESSFUL);
	}

}
