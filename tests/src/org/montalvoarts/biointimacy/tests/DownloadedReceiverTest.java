package org.montalvoarts.biointimacy.tests;

import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.sync.EvernoteClient;
import org.montalvoarts.biointimacy.sync.DownloadedReceiver;

import com.evernote.edam.notestore.NoteStore.Client;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.test.AndroidTestCase;
import android.util.Log;

/**
 * Extend provider test so that we can make use of one
 */
public class DownloadedReceiverTest extends AndroidTestCase {
    
    private static final String TAG = "TEST";

	private DownloadManager mDownloader;

	
    @Override
    protected void setUp() throws Exception {
        // TODO Auto-generated method stub
        super.setUp();
		mDownloader = (DownloadManager) mContext
				.getSystemService(Context.DOWNLOAD_SERVICE);
    }


	public void testReceive() throws InterruptedException {
		// enqueue a download
		EvernoteClient client = new EvernoteClient(mContext);
		Client c = client.connect();
		client.checkAndSetLastUsn(c);
		long downId = client
				.downloadNoteThumbnail(EvernoteClientTest.KNOWN_NOTE_GUID, Util.EXT_DIR_NAME);
		assertTrue(downId > 0);

		Intent intent = createFakeDownloadIntent(downId);
		assertNotNull(intent);

		DownloadedReceiver receiver = new DownloadedReceiver();
		receiver.onReceive(mContext, intent);
	}

	private Intent createFakeDownloadIntent(long downId) throws InterruptedException {

		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterById(downId);

		for (int i = 0; i < 10; i++) {
			Thread.sleep(5000);
			Cursor c = mDownloader.query(query);
			if (c.getCount() > 0 && c.moveToNext()) {
				int status = c.getInt(c
						.getColumnIndexOrThrow(DownloadManager.COLUMN_STATUS));
				Log.d(TAG, "Got status " + status + " and uri " + mDownloader.getUriForDownloadedFile(downId));
				if (status == DownloadManager.STATUS_SUCCESSFUL) {
					Intent intent = new Intent(
							DownloadManager.ACTION_DOWNLOAD_COMPLETE);
					intent.putExtra(DownloadManager.EXTRA_DOWNLOAD_ID, downId);
					return intent;
				}
			}
		}
		return null;
	}
}
