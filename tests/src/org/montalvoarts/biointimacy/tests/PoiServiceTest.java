package org.montalvoarts.biointimacy.tests;

import java.util.HashMap;
import java.util.Map;

import org.montalvoarts.biointimacy.data.Constants;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiProvider;
import org.montalvoarts.biointimacy.sync.EvernoteClient;
import org.montalvoarts.biointimacy.sync.PoiService;
import org.montalvoarts.biointimacy.sync.PoiService.Cmd;
import org.montalvoarts.biointimacy.tests.PoiProviderTest.PoiBean;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.test.IsolatedContext;
import android.test.ServiceTestCase;
import android.test.mock.MockContentResolver;
import android.text.TextUtils;
import android.util.Log;

import com.evernote.edam.notestore.NoteStore.Client;
import com.evernote.edam.notestore.SyncChunk;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;

public class PoiServiceTest extends ServiceTestCase<PoiService> {

	public PoiServiceTest() {
		super(PoiService.class);

	}

	protected void setUp() throws Exception {
		super.setUp();

		// Make PoiProvider available during testing
		MockContentResolver resolver = new MockContentResolver();

		IsolatedContext mockContext = new IsolatedContext(resolver,
				getSystemContext());

		PoiProvider provider = new PoiProvider();
		provider.attachInfo(mockContext, null);
		resolver.addProvider(Constants.AUTHORITY, provider);

		// Inject a context that knows about our provider
		setContext(mockContext);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		getContext().getContentResolver().delete(PoiContract.CONTENT_URI, null,
				null);
	}

	private static final String FAKE_THUMBURI = "content://downloads/my_downloads/1054";
	private static final String TAG = "TEST";

//	public void testLocalLocal() throws InterruptedException {
//		PoiHelper h = PoiHelper.SINGLETON;
//
//		startService(Cmd.POI_LOAD_LOCAL.formIntent(getContext()));
//		Thread.sleep(6000);
//		
//		// load the local ones
//		Poi whatIs = h.create(getContext(), h.formUriFromNoteGuid(PoiLocal.GUID_WHATIS_BIO));
//		assertNotNull(whatIs);
//		assertEquals(PoiLocal.GUID_WHATIS_BIO, whatIs.getString(Col.GUID));
//		assertTrue(!TextUtils.isEmpty(whatIs.getString(Col.BODY)));
//		assertTrue(!TextUtils.isEmpty(whatIs.getString(Col.TITLE)));
//		
//		Poi howUse= h.create(getContext(), h.formUriFromNoteGuid(PoiLocal.GUID_HOWUSE_APP));
//		assertNotNull(howUse);
//		assertEquals(PoiLocal.GUID_HOWUSE_APP, howUse.getString(Col.GUID));
//		assertTrue(!TextUtils.isEmpty(howUse.getString(Col.BODY)));
//		assertTrue(!TextUtils.isEmpty(howUse.getString(Col.TITLE)));
//	}
	
	public void testUpdateThumb() throws InterruptedException {
		// Create a POI w/ a downloadId
		ContentResolver resolver = getContext().getContentResolver();
		Uri uri = createNewlyInsertedPoi(resolver, 100);
		Uri uri2 = createNewlyInsertedPoi(resolver, 200);

		Poi poi = PoiHelper.SINGLETON.create(getContext(), uri);
		assertNotNull(poi);
		assertEquals(100, poi.getInteger(Col.THUMB_DWNLD_ID));
		assertTrue(TextUtils.isEmpty(poi.getString(Col.THUMB_URI)));

		// Update the POI
		Intent start = PoiService.Cmd.POI_UPDATE_THUMBURI.formIntent(getContext());
		start.putExtra(PoiService.EXTRA_LONG_DOWNLOADID, 100);
		start.putExtra(PoiService.EXTRA_STR_THUMBURI, FAKE_THUMBURI);
		startService(start);

		Thread.sleep(5000);

		// Confirm results in DB
		poi = PoiHelper.SINGLETON.create(getContext(), uri);
		assertNotNull(poi);
		assertEquals(FAKE_THUMBURI, poi.getString(Col.THUMB_URI));
		assertTrue(poi.getInteger(Col.THUMB_DWNLD_ID) <= 0);

		// Second POI shouldn't have been updated
		poi = PoiHelper.SINGLETON.create(getContext(), uri2);
		assertNotNull(poi);
		assertTrue(TextUtils.isEmpty(poi.getString(Col.THUMB_URI)));
		assertTrue(poi.getInteger(Col.THUMB_DWNLD_ID) > 0);
	}

	public void testUpdateBody() throws InterruptedException {

		EvernoteClient evernote = new EvernoteClient(getContext());
		evernote.reset();

		// Assume client can be reused for lifetime of service
		Client client = evernote.connect();
		evernote.checkAndSetLastUsn(client);

		Note firstNote = EvernoteClientTest.getFirstNote(evernote, client);
		assertNotNull(firstNote);
		int contentLen = firstNote.getContentLength();
		assertTrue(contentLen > 0);
		String noteGuid = firstNote.getGuid();
		assertTrue(!TextUtils.isEmpty(noteGuid));

		// Create a POI w/ empty body
		Poi toInsertPoi = PoiHelper.SINGLETON.create(firstNote);
		Uri uri = PoiHelper.SINGLETON.insert(getContext(), toInsertPoi);

		// Query from db and confirm empty body
		Poi poi = PoiHelper.SINGLETON.create(getContext(), uri);
		assertNotNull(poi);
		assertTrue(TextUtils.isEmpty(poi.getString(Col.BODY)));

		// Update the POI's body
		Intent start = PoiService.Cmd.POI_UPDATE_NOTE.formIntent(getContext());
		start.putExtra(PoiService.EXTRA_STR_POIURI, uri.toString());
		startService(start);

		Thread.sleep(5000);

		// Confirm results
		poi = PoiHelper.SINGLETON.create(getContext(), uri);
		assertNotNull(poi);
		assertEquals(noteGuid, poi.getString(Col.GUID));
		String body = poi.getString(Col.BODY);
		assertTrue(!TextUtils.isEmpty(body));
		assertEquals(contentLen, body.length());
	}

	public void testSync() throws InterruptedException {

		EvernoteClient evernote = new EvernoteClient(getContext());
		evernote.reset();

		// Assume client can be reused for lifetime of service
		Client client = evernote.connect();
		evernote.checkAndSetLastUsn(client);

		// get initial list of notes before the sync
		SyncChunk chunk = evernote.getSyncChunk(client);
		evernote.reset();

		Map<String, Poi> expected = getExpectedPoi(chunk);

		// Do the sync and save POI to db
		startService(Cmd.POI_SYNC.formIntent(getContext()));

		Thread.sleep(15000);

		// Iterate through the POI in the db
		Cursor c = getContext().getContentResolver().query(
				PoiContract.CONTENT_URI, Col.proj(), null, null, null);
		// Add one for the local POI created from local json
		assertEquals(expected.size() + 1, c.getCount());
		while (c != null && c.moveToNext()) {
			String localGuid = c.getString(Col.GUID.ind);
			Poi match = expected.get(localGuid);
			if (match == null) {
				continue;
			}
			assertEquals(match.getString(Col.GUID), localGuid);
			assertEquals(match.getString(Col.TITLE), c.getString(Col.TITLE.ind));
			assertEquals(match.getDouble(Col.LAT), c.getDouble(Col.LAT.ind));
			assertEquals(match.getDouble(Col.LONG), c.getDouble(Col.LONG.ind));
			Log.d(TAG,"Successfully inserted POI with title, " + c.getString(Col.TITLE.ind));
		}

	}

	private Map<String, Poi> getExpectedPoi(SyncChunk chunk) {
		Map<String, Poi> expected = new HashMap<String, Poi>();
		for (Note note : chunk.getNotes()) {
			if (note.isSetDeleted() || note.getDeleted() > 0) {
				continue;
			}
			Poi poi = PoiHelper.SINGLETON.create(note);
			expected.put(note.getGuid(), poi);
			Log.d(TAG,"Got note " + note.getResourcesSize());
			if (note.getResourcesSize() > 0) {
				Resource res = note.getResources().get(0);
				assertNotNull(res.getData());
				Log.d(TAG,"got data size " + res.getData().getSize());
				Log.d(TAG,"got data hash " + res.getData().getBodyHash());				
			}
		}
		return expected;
	}

	private Uri createNewlyInsertedPoi(ContentResolver resolver, int downloadId) {
		PoiBean poi = PoiProviderTest.createSample();
		poi.val.put(Col.THUMB_DWNLD_ID.col, downloadId);
		poi.val.putNull(Col.THUMB_URI.col);
		poi.val.putNull(Col.BODY.col);
		Uri uri = resolver.insert(PoiContract.CONTENT_URI, poi.val);
		assertNotNull(uri);
		return uri;
	}

}
