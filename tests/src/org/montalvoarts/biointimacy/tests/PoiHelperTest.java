package org.montalvoarts.biointimacy.tests;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.montalvoarts.biointimacy.data.Constants;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiProvider;
import org.montalvoarts.biointimacy.data.PoiRes;
import org.montalvoarts.biointimacy.data.PoiResContract;
import org.montalvoarts.biointimacy.tests.PoiProviderTest.PoiBean;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentUris;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.test.IsolatedContext;
import android.test.mock.MockContentResolver;
import android.util.Log;

public class PoiHelperTest extends AndroidTestCase {
	private static final String ENML = "<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE en-note SYSTEM 'http://xml.evernote.com/pub/enml2.dtd'><en-note><div><a shape='rect' href='http://www.google.com'><en-media hash='1fe7d715b75e311d5b7cdc9fb2d29e27' type='image/jpeg'>foobar</en-media></a></div><div><div>1) �Leonardo wrote with it, Van Gogh drew with it: �OAK GALL INK</div><div><br clear='none'/></div><div>2) <span>�Look up into the branches of the oak tree behind you. Can you see�</span>the oak galls? The tree produces a gall in response to a Gall Wasp egg, which the tiny Wasp injects into an oak�stem. During the nine-months it spends in its gall nursery, the <span>egg becomes a lava, pupates, and develops into an adult,�</span>before burrowing a tiny hole to the outside world.</div><div><br clear='none'/></div><div>The galls, which darken over time, are full of tannin. A mixture of crushed gall, iron sulphate, water (or sometimes wine), and gum arabic�makes the rich black ink that Europeans used for a thousand years, and which the Founding Fathers used to write the the US Declaration of Independence.</div><div><br clear='none'/></div><div>Find an Oak Gall ink recipe here:�</div><div>�<a shape='rect' href='http://endless-swarm.com/?p=834' target='_blank'>http://endless-swarm.com/?p=834</a></div><div><br clear='none'/></div><div>3)�Europe's ink of choice for a thousand years, oak gall ink was used to make medieval manuscripts and to sign the U.S. Declaration of Independence. �The spherical &quot;wasp nurseries&quot; an oak produced when a gall wasp lays her eggs in the tree.�</div><div>4) Image attached</div><div>5)�Video of gall wasp laying egg:<p><a shape='rect' href='http://www.youtube.com/watch?v=CzXccvoJThI' target='_blank'><span>http://www.youtube.com/watch?v=CzXccvoJThI</span></a></p></div></div></en-note>";
	private static final String ENML2 = "<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE en-note SYSTEM 'http://xml.evernote.com/pub/enml2.dtd'><en-note><div><a shape='rect' href='http://www.google.com'><en-media hash='1fe7d715b75e311d5b7cdc9fb2d29e27' type='image/jpeg'></en-media></a></div><div><div>1) �Leonardo wrote with it, Van Gogh drew with it: �OAK GALL INK</div><div><br clear='none'/></div><div>2) <span>�Look up into the branches of the oak tree behind you. Can you see�</span>the oak galls? The tree produces a gall in response to a Gall Wasp egg, which the tiny Wasp injects into an oak�stem. During the nine-months it spends in its gall nursery, the <span>egg becomes a lava, pupates, and develops into an adult,�</span>before burrowing a tiny hole to the outside world.</div><div><br clear='none'/></div><div>The galls, which darken over time, are full of tannin. A mixture of crushed gall, iron sulphate, water (or sometimes wine), and gum arabic�makes the rich black ink that Europeans used for a thousand years, and which the Founding Fathers used to write the the US Declaration of Independence.</div><div><br clear='none'/></div><div>Find an Oak Gall ink recipe here:�</div><div>�<a shape='rect' href='http://endless-swarm.com/?p=834' target='_blank'>http://endless-swarm.com/?p=834</a></div><div><br clear='none'/></div><div>3)�Europe's ink of choice for a thousand years, oak gall ink was used to make medieval manuscripts and to sign the U.S. Declaration of Independence. �The spherical &quot;wasp nurseries&quot; an oak produced when a gall wasp lays her eggs in the tree.�</div><div>4) Image attached</div><div>5)�Video of gall wasp laying egg:<p><a shape='rect' href='http://www.youtube.com/watch?v=CzXccvoJThI' target='_blank'><span>http://www.youtube.com/watch?v=CzXccvoJThI</span></a></p></div></div></en-note>";
	private static final String ENML3 = "<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE en-note SYSTEM 'http://xml.evernote.com/pub/enml2.dtd'><en-note><div><a shape='rect' href='http://www.google.com'><en-media hash='1fe7d715b75e311d5b7cdc9fb2d29e27' type='image/jpeg'/></a></div><div><div>1) �Leonardo wrote with it, Van Gogh drew with it: �OAK GALL INK</div><div><br clear='none'/></div><div>2) <span>�Look up into the branches of the oak tree behind you. Can you see�</span>the oak galls? The tree produces a gall in response to a Gall Wasp egg, which the tiny Wasp injects into an oak�stem. During the nine-months it spends in its gall nursery, the <span>egg becomes a lava, pupates, and develops into an adult,�</span>before burrowing a tiny hole to the outside world.</div><div><br clear='none'/></div><div>The galls, which darken over time, are full of tannin. A mixture of crushed gall, iron sulphate, water (or sometimes wine), and gum arabic�makes the rich black ink that Europeans used for a thousand years, and which the Founding Fathers used to write the the US Declaration of Independence.</div><div><br clear='none'/></div><div>Find an Oak Gall ink recipe here:�</div><div>�<a shape='rect' href='http://endless-swarm.com/?p=834' target='_blank'>http://endless-swarm.com/?p=834</a></div><div><br clear='none'/></div><div>3)�Europe's ink of choice for a thousand years, oak gall ink was used to make medieval manuscripts and to sign the U.S. Declaration of Independence. �The spherical &quot;wasp nurseries&quot; an oak produced when a gall wasp lays her eggs in the tree.�</div><div>4) Image attached</div><div>5)�Video of gall wasp laying egg:<p><a shape='rect' href='http://www.youtube.com/watch?v=CzXccvoJThI' target='_blank'><span>http://www.youtube.com/watch?v=CzXccvoJThI</span></a></p></div></div></en-note>";
	private static final String TAG = "TEST";

	private static final String JSON = "{'guid': 'GUID_WHATIS_BIO','title': 'What is Biointimacy?','thumb_uri': 'android.resource:///org.montalvoarts.biointimacy/drawable/thumb_whatis_bio','body':'hello world'}";

	protected void setUp() throws Exception {
		super.setUp();

		// Enable provider functionality
		MockContentResolver resolver = new MockContentResolver();
		IsolatedContext c = new IsolatedContext(resolver, getContext());
		PoiProvider provider = new PoiProvider();
		provider.attachInfo(c, null);
		resolver.addProvider(Constants.AUTHORITY, provider);

		setContext(c);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		getContext().getContentResolver().delete(PoiContract.CONTENT_URI, null,
				null);
	}

	public void testFormPoiFromJson() throws JSONException {
		JSONObject singleLocal = new JSONObject(JSON);
		Poi poi = PoiHelper.SINGLETON.create(singleLocal);
		assertTrue(poi != null && poi.isValid());
		assertEquals("GUID_WHATIS_BIO", poi.getString(Col.GUID));
		assertEquals("What is Biointimacy?", poi.getString(Col.TITLE));
	}

	public void testGetByNoteGuid() {
		PoiBean b = PoiProviderTest.createSample();
		getContext().getContentResolver()
				.insert(PoiContract.CONTENT_URI, b.val);
		String guid = b.val.getAsString(Col.GUID.col);

		Uri newUri = PoiHelper.SINGLETON.formUriFromNoteGuid(guid);
		Poi poi = PoiHelper.SINGLETON.create(getContext(), newUri);

		assertNotNull(poi);
		assertTrue(poi.isValid());
		assertEquals(guid, poi.getString(Col.GUID));
	}


	public void testListResources() {
		PoiBean b = PoiProviderTest.createSample();
		Uri uri = getContext().getContentResolver().insert(
				PoiContract.CONTENT_URI, b.val);
		long rowId = ContentUris.parseId(uri);
		
		createPoiRes(uri, "fooguid1","hash");
		createPoiRes(uri, "fooguid2","hash");
		createPoiRes(uri, "fooguid3","hash");
		
		List<PoiRes> result = PoiHelper.SINGLETON.listResources(getContext(), rowId);
		assertEquals(3,result.size());
		for (PoiRes res:result) {
			assertTrue(res.isValid());
			assertEquals("hash",res.getString(PoiResContract.Col.HASH));
		}

		
	}

	private void createPoiRes(Uri uri, String guid, String hash) {
		PoiRes res = new PoiRes();
		res.put(PoiResContract.Col.GUID, guid);
		res.put(PoiResContract.Col.HASH,hash);
		PoiHelper.SINGLETON.persist(getContext(), uri, res);
	}

	
	public void testCreate() {
		PoiBean b = PoiProviderTest.createSample();
		Uri uri = getContext().getContentResolver().insert(
				PoiContract.CONTENT_URI, b.val);
		String guid = b.val.getAsString(Col.GUID.col);

		Poi poi = PoiHelper.SINGLETON.create(getContext(), uri);
		assertNotNull(poi);
		assertTrue(poi.isValid());
		assertEquals(guid, poi.getString(Col.GUID));
	}

	public void testPersist() {
		Poi poi = new Poi();
		poi.put(Col.GUID, "foo");
		assertNull(PoiHelper.SINGLETON.persist(getContext(), poi));

		poi.put(Col.TITLE, "f");
		poi.put(Col.LAT, 100.01);
		poi.put(Col.LONG, 100.01);
		assertNotNull(PoiHelper.SINGLETON.persist(getContext(), poi));
	}

	public void assertReadEnml(String enml) throws IOException,
			XmlPullParserException {
//		String html = PoiHelper.SINGLETON.extractResHash2IntentMap(ENML);
//
//		Log.d(TAG, "out converted " + html);
//		assertNotNull(html);
//		assertTrue(html.contains("http://foobar.com"));
//		assertTrue(!html.contains("en-media"));
//		if (enml.contains("</en-media>")) {
//			assertTrue(html.contains("</img>"));
//		}
	}

	public void testAllEnm() throws IOException, XmlPullParserException {
		String[] tests = new String[] { ENML, ENML2, ENML3 };

		for (String test : tests) {
			Log.d(TAG, "For input " + test);
			assertReadEnml(test);
		}

	}

}
