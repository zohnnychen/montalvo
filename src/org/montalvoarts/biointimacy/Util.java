package org.montalvoarts.biointimacy;

import java.util.HashMap;
import java.util.Map;

import org.montalvoarts.biointimacy.view.TypefaceSpan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.LocationManager;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

public class Util {
	
	private static final MimeTypeMap sMimeMap = MimeTypeMap.getSingleton();


	public static final String APP_PREF = "app_pref";
	public static final String PREF_BOOL_ISSHOW_GOOGLEDIALOG = "APP_PREF_ISSHOW_GOOGLEDIALOG";


	/**
	 * Package-level or summary log tags.
	 * <p>
	 * User can filter log statements to just those Logs that belong to one of
	 * these Summary tags by doing something like:
	 * <code>setprop log.tag.[SUMMARY] VERBOSE|DEBUG</code> where [SUMMARY] is
	 * <code>Tag.Bio_Data.name()</code>.
	 * <p>
	 * e.g <code>setprop log.tag.Bio_Data DEBUG</code> will print out all those
	 * Log statements from that Java package.
	 */
	public enum Tag {
		// Make sure the names here don't collide with those of {@link Tag}
		Bio_Data, Bio_Act, Bio_Sync;

		/**
		 * Prefix argument tag with the Tag's name.
		 */
		public String prefix(String tag) {
			return name() + DELIM + tag;
		}

		static final char DELIM = '_';
	}

	// lives in /assets/fonts
	public static final String TYPEFACE_TITLE = "fonts/Quicksand-Regular.otf";
	public static final String TYPEFACE_BODY = "fonts/UglyQua.ttf";
	private static final String TAG = "Bio_Util";
	

	public static final String EXT_DIR_NAME = "montalvo_pics";


	private static Map<String, Typeface> sTypeFaces = new HashMap<String, Typeface>();

	public static SpannableString withCustomTypeface(Context context, String src, String typeface) {
		SpannableString s = new SpannableString(src);
		s.setSpan(new TypefaceSpan(context, typeface), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return s;
	}

	public static void setTypeface(Context context, TextView view, String typeface) {
		Typeface tf = getCustomTypeface(context, typeface);
		view.setTypeface(tf);
		// Make it anti-aliased
		view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);

	}
	
	/**
	 * @param path
	 *            Path relative to the "<project>/assets/" folder
	 * @return
	 */
	private static Typeface getCustomTypeface(Context context, String path) {
		Typeface face = sTypeFaces.get(path);
		if (face == null) {
			face = Typeface.createFromAsset(context.getAssets(), path);
			sTypeFaces.put(path, face);
		}
		return face;
	}
	
	/**
	 * 
	 * @param url URL may contain a reference to a file name with an extension. Infer the mimetype from the extension
	 * @return
	 */
	public static Intent intentFromUrl(String url) {
		// Always view it
		Uri uri = Uri.parse(url);
		Intent i = new Intent(Intent.ACTION_VIEW);
		String ext = MimeTypeMap.getFileExtensionFromUrl(url);
		if (!TextUtils.isEmpty(ext) && sMimeMap.hasExtension(ext)) {
			String mimeType = sMimeMap.getMimeTypeFromExtension(ext);
			if (!TextUtils.isEmpty(mimeType)) {
				i.setDataAndType(uri, mimeType);
			}
		} else {
			i.setData(uri);
		}
		return i;
	}

	public static boolean isGpsEnabled(Context context) {
		try {
			LocationManager mgr = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			if (mgr == null) {
				return false;
			}
			Criteria c= new Criteria();
			c.setAccuracy(Criteria.ACCURACY_FINE);
			String prov = mgr.getBestProvider(c, true);
			Log.d(TAG,"Determined best location provider to be " + prov);
			return prov != null && prov.equals(LocationManager.GPS_PROVIDER);
		} catch (Exception e) {
			Log.w(TAG,"Unable to get location manger", e);
			return false;
		}
	}
}
