package org.montalvoarts.biointimacy;


public class Analytics {

    public enum Timing {
        SYNC_FINISH("SYNC"),
        CLEAN_SYNC_FINISH("SYNC");
        public String category;
        private Timing(String category) {
            this.category=category;
        }
    }

    public enum Event {
        FIRST_TIME_LAUNCH_APP("USER_ACTION"), 
        VIEW_POI("USER_ACTION"), 
        SHARE_POI("USER_ACTION");
 
        public String category;
         
        private Event (String category) {
            this.category = category;
        }
    }
}
