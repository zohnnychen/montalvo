package org.montalvoarts.biointimacy.view;

import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.sync.PoiService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;

public class ThumbCache {
	private static final int BITMAP_WID = 200;
	private static final int BITMAP_HEIGHT = 200;
	private static final String TAG = Util.Tag.Bio_Act.prefix("Cache");
	private LruCache<String, Bitmap> mMemoryCache;
	private BroadcastReceiver mUriUpdatedReceiver;
	private Context mContext;

	public ThumbCache(final Context context) {
		mContext = context;
		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;

		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};

		mUriUpdatedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(PoiService.ACTION_UPDATED_THUMBNAILS)) {
					String guid= intent.getStringExtra(PoiService.EXTRA_STRING_OWNERGUID);
					Uri thumbUri = intent.getParcelableExtra(PoiService.EXTRA_URI_LOCALTHUMB);
					if (TextUtils.isEmpty(guid) || thumbUri == null) {
						Log.w(TAG, "Aborting. Unable to get URI from intent "
								+ intent);
						return;
					}
					Log.v(TAG, "Received signal that thumbnail for URI " + guid
							+ " has been downloaded");
					initMapping(guid, thumbUri);
				}

			}
		};
		IntentFilter filter = new IntentFilter(PoiService.ACTION_UPDATED_THUMBNAILS);
		mContext.registerReceiver(mUriUpdatedReceiver, filter);
	}

	public void initWithAllPoi(Context context) {
		Cursor c = null;
		try {
			c = context.getContentResolver().query(PoiContract.CONTENT_URI,
					Col.proj(), Col.THUMB_DWNLD_ID.col + " IS NULL", null, null);
			Log.v(TAG, "Cache has " + mMemoryCache.size()
					+ " entries and poi w/o downloadId set is: " + c.getCount());
			while (c != null && c.moveToNext()) {
				String thumbUriStr = c.getString(Col.THUMB_URI.ind);
				String guid = c.getString(Col.GUID.ind);
				if (!TextUtils.isEmpty(guid) && !TextUtils.isEmpty(thumbUriStr)) {
					Bitmap bm = mMemoryCache.get(guid);
					if (bm != null) {
						// skip
						continue;
					}
					bm = PoiHelper.SINGLETON.decodeSampledBitmap(context,
							Uri.parse(thumbUriStr), BITMAP_WID, BITMAP_HEIGHT);
					if (bm != null) {
						addBitmapToMemoryCache(guid, bm);
					}
				}
			}
			Log.v(TAG, "Filled up thumb cache with " + mMemoryCache.size()
					+ " entries");
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}
	
	private void initMapping(String key, Uri thumbUri) {
		Bitmap bm = mMemoryCache.get(key);
		if (bm != null) {
			// skip
			return;
		}
		bm = PoiHelper.SINGLETON.decodeSampledBitmap(mContext, thumbUri,
				BITMAP_WID, BITMAP_HEIGHT);
		if (bm != null) {
			addBitmapToMemoryCache(key, bm);
		}
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			Log.v(TAG, "Added bitmap for key " + key);
			mMemoryCache.put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	public void clear() {
		mMemoryCache.evictAll();
		if (mUriUpdatedReceiver != null) {
			mContext.unregisterReceiver(mUriUpdatedReceiver);
		}
	}

}
