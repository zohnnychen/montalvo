package org.montalvoarts.biointimacy.view;

import org.montalvoarts.biointimacy.R;
import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.fima.cardsui.objects.Card;

/**
 * Card with POI image, title and hook set
 */
public class PoiCard extends Card {

	private Context mContext;
	private Poi mPoi;
	private ThumbCache mCache;

	public PoiCard(Context context, Poi poi, ThumbCache cache) {
		mContext = context;
		mPoi = poi;
		mCache = cache;
		if (cache == null || mPoi == null || !mPoi.isValid()) {
			throw new IllegalArgumentException("Encountered invalid POI" + poi);
		}


	}

	@Override
	public View getCardContent(Context context) {
		// Expensive op to get description
		View view = LayoutInflater.from(context).inflate(R.layout.poi_card,
				null);

		String titleStr = mPoi.getString(Col.TITLE);
		if (!TextUtils.isEmpty(titleStr)) {
			TextView titleV =(TextView) view.findViewById(R.id.title);
			Util.setTypeface(mContext, titleV, Util.TYPEFACE_TITLE);
			titleV.setText(titleStr);
		}

		String body = mPoi.getString(Col.BODY);
		if (!TextUtils.isEmpty(body)) {
			Spanned bodyStr = Html.fromHtml(body);
			TextView bodyText = (TextView)view.findViewById(R.id.description);
			Util.setTypeface(mContext, bodyText, Util.TYPEFACE_BODY);
			bodyText.setText(bodyStr);
		}

		Bitmap image = mCache.getBitmapFromMemCache(mPoi.getString(Col.GUID));
		if (image != null) {
			((ImageView) view.findViewById(R.id.list_badge))
					.setImageBitmap(image);
		}
		
		float prox = mPoi.getFloat(Col.PROXIMITY_METERS);
		if (prox > 0) {
			String dist = PoiHelper.SINGLETON.prettyEnglishDistance(prox, mContext);
			TextView proxV =(TextView) view.findViewById(R.id.proximity);
			Util.setTypeface(mContext, proxV, Util.TYPEFACE_TITLE);
			proxV.setText(dist);
		}

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Uri uri = ContentUris.withAppendedId(PoiContract.CONTENT_URI,
						mPoi.getInteger(Col.ID));
				mContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
			}
		});
		return view;
	}

}
