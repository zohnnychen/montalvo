package org.montalvoarts.biointimacy;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;

public abstract class GoogleConnHelper implements ConnectionCallbacks,
		OnConnectionFailedListener {

	private static final String TAG = Util.Tag.Bio_Act.prefix("Google");
	private Activity mActivity;
	
	/*
	 * Define a request code to send to Google Play services This code is
	 * returned in Activity.onActivityResult
	 */
	public final static int REQCODE_RESOLVE_CONNECTION_FAILURE = 9000;

	public GoogleConnHelper(Activity activity) {
		mActivity = activity;
	}
	
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		switch (requestCode) {
		case REQCODE_RESOLVE_CONNECTION_FAILURE:
			switch (resultCode) {
			// If Google Play services resolved the problem
			case Activity.RESULT_OK:
				Log.d(TAG, "Google play services resolved connection problem");
				break;
			default:
				Log.w(TAG, "Unrecognized result from Google Play services"
						+ resultCode);
				break;
			}
		default:
			Log.w(TAG, "Unrecognized request code " + requestCode);
			break;
		}
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.w(TAG, "Client connection failure");
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(mActivity,
						REQCODE_RESOLVE_CONNECTION_FAILURE);
			} catch (Exception e) {
				Log.e(TAG, "Unable to handle connection failure", e);
			}
		} else {
			// If no resolution is available, display a dialog to the user with
			// the error.
			showErrorDialog(connectionResult.getErrorCode());
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		if (servicesConnected()) {
			onSuccessfullyConnected();
		}
	}
	
	/**
	 * Callback invoked when connection to Google Play services is successful
	 */
	abstract public void onSuccessfullyConnected();

	@Override
	public void onDisconnected() {
		Log.d(TAG, "Disconnected from Google Play services");
	}

	/**
	 * Show a dialog returned by Google Play services for the connection error
	 * code
	 * 
	 * @param errorCode
	 *            An error code returned from onConnectionFailed
	 */
	private void showErrorDialog(int errorCode) {
	
		try {
			Log.w(TAG, "Got error dialog code " + errorCode);
			// Get the error dialog from Google Play services
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					errorCode, mActivity, REQCODE_RESOLVE_CONNECTION_FAILURE);
	
			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
	
				// Create a new DialogFragment in which to show the error dialog
				GenericDialogFragment errorFragment = new GenericDialogFragment();
	
				// Set the dialog in the DialogFragment
				errorFragment.setDialog(errorDialog);
	
				// Show the error dialog in the DialogFragment
				errorFragment.show(mActivity.getFragmentManager(), TAG);
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to show error dialog", e);
			Toast.makeText(mActivity, R.string.enable_location_desc, Toast.LENGTH_LONG)
					.show();
		}
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {
	
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(mActivity);
	
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					mActivity, 0);
			if (dialog != null) {
				GenericDialogFragment errorFragment = new GenericDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(mActivity.getFragmentManager(), TAG);
			}
			return false;
		}
	}
}
