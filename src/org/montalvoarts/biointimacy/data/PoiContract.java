package org.montalvoarts.biointimacy.data;

import java.util.ArrayList;

import org.montalvoarts.biointimacy.Util;

import android.net.Uri;
import edu.mit.mobile.android.content.ContentItem;
import edu.mit.mobile.android.content.ForeignKeyManager;
import edu.mit.mobile.android.content.ProviderUtils;
import edu.mit.mobile.android.content.UriPath;
import edu.mit.mobile.android.content.column.DBColumn;
import edu.mit.mobile.android.content.column.DBColumnType;
import edu.mit.mobile.android.content.column.DoubleColumn;
import edu.mit.mobile.android.content.column.FloatColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;

/**
 * Abstraction of the data available in the Provider.
 */
@UriPath(PoiContract.PATH)
public class PoiContract implements ContentItem {

	/**
	 * The table name offered by this provider
	 */
	public static final String PATH = "poi";

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri CONTENT_URI = ProviderUtils.toContentUri(
			Constants.AUTHORITY, PATH);

	/*
	 * Column definitions
	 */
	@DBColumn(type = TextColumn.class, notnull = true)
	public static final String GUID = "guid";

	@DBColumn(type = TextColumn.class, notnull = true)
	public static final String TITLE = "title";

	@DBColumn(defaultValueInt = 0, notnull = true, type = DoubleColumn.class)
	public static final String LAT = "latitude";

	@DBColumn(defaultValueInt = 0, notnull = true, type = DoubleColumn.class)
	public static final String LONG = "longitude";

	@DBColumn(type = IntegerColumn.class)
	public static final String NOTE_USN = "note_usn";

	@DBColumn(type = TextColumn.class)
	public static final String THUMB_URI = "thumb_uri";

	@DBColumn(type = TextColumn.class)
	public static final String BODY = "body";

	@DBColumn(type = IntegerColumn.class)
	public static final String THUMB_DWNLD_ID = "thumb_dwnld_id";

	@DBColumn(type = FloatColumn.class)
	public static final String PROXIMITY_METERS = "proximity_meters";

	@DBColumn(type = TextColumn.class)
	public static final String PUBLIC_URI = "public_uri";

	public static final String TAG = Util.Tag.Bio_Data.prefix("Poi");

	static {
		PoiContract.class.getFields();
	}

	// This is a helpful tool connecting back to the "child" of this object.
	// This is similar
	// to Django's relation manager, although we need to define it ourselves.
	public static final ForeignKeyManager RESOURCES = new ForeignKeyManager(
			PoiResContract.class);

	public enum Col {
		ID(PoiContract._ID, 0, IntegerColumn.class), GUID(PoiContract.GUID, 1,
				TextColumn.class, true), TITLE(PoiContract.TITLE, 2,
				TextColumn.class, true), LAT(PoiContract.LAT, 3,
				DoubleColumn.class), LONG(PoiContract.LONG, 4,
				DoubleColumn.class), NOTE_USN(PoiContract.NOTE_USN, 5,
				IntegerColumn.class), THUMB_URI(PoiContract.THUMB_URI, 6,
				TextColumn.class), BODY(PoiContract.BODY, 7, TextColumn.class), THUMB_DWNLD_ID(
				PoiContract.THUMB_DWNLD_ID, 8, IntegerColumn.class), PROXIMITY_METERS(
				PoiContract.PROXIMITY_METERS, 9, FloatColumn.class), PUBLIC_URI(PoiContract.PUBLIC_URI, 10,
						TextColumn.class);

		Col(String colName, int index, Class<? extends DBColumnType<?>> type,
				boolean required) {
			this.col = colName;
			this.ind = index;
			this.type = type;
			this.required = required;
		}

		Col(String colName, int index, Class<? extends DBColumnType<?>> type) {
			this(colName, index, type, false);
		}

		public Class<? extends DBColumnType<?>> type;
		public String col;
		public int ind;
		public boolean required;

		/**
		 * Returns in order projection of column names
		 */
		public static String[] proj() {
			ArrayList<String> result = new ArrayList<String>();
			for (Col p : values()) {
				result.add(p.col);
			}
			return result.toArray(new String[] {});
		}

	}
}
