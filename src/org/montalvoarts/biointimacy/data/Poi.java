package org.montalvoarts.biointimacy.data;

import org.montalvoarts.biointimacy.data.PoiContract.Col;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import edu.mit.mobile.android.content.column.BooleanColumn;
import edu.mit.mobile.android.content.column.DoubleColumn;
import edu.mit.mobile.android.content.column.FloatColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;
import edu.mit.mobile.android.content.column.TimestampColumn;

/**
 * Helper data object that encapsulates the contents of a single POI entity.
 * Values are found in the {@link #getVals()} object.
 * <p>
 * Can be intialized by a Cursor pointing to a row in the POI database.
 */
public class Poi {
	// Each value's key is Col#col
	private ContentValues mVal;

	public Poi() {
		mVal = new ContentValues();
	}

	public boolean isValid() {
		for (Col col : Col.values()) {
			if (col.required && !mVal.containsKey(col.col)) {
				return false;
			}
		}
		return true;
	}

	public String toString() {
		return mVal.toString();
	}

	ContentValues getVals() {
		return mVal;
	}

	public String getString(Col col) {
		if (!col.type.equals(TextColumn.class)) {
			return null;
		}
		String val = mVal.getAsString(col.col);
		if (TextUtils.isEmpty(val)) {
			return null;
		}
		return val;
	}

	public int getInteger(Col col) {
		if (!col.type.equals(IntegerColumn.class)) {
			return -1;
		}
		Integer val = mVal.getAsInteger(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}

	public float getFloat(Col col) {
		if (!col.type.equals(FloatColumn.class)) {
			return -1;
		}
		Float val = mVal.getAsFloat(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}

	public double getDouble(Col col) {
		if (!col.type.equals(DoubleColumn.class)) {
			return -1;
		}
		Double val = mVal.getAsDouble(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}

	public void put(Col col, String val) {
		mVal.put(col.col, val);
	}

	public void put(Col col, int val) {
		mVal.put(col.col, val);
	}

	public void put(Col col, double val) {
		mVal.put(col.col, val);
	}

	void put(Col col, Object val) {
		if (val instanceof Integer && col.type.equals(IntegerColumn.class)) {
			mVal.put(col.col, (Integer) val);
		} else if (val instanceof Double
				&& col.type.equals(DoubleColumn.class)) {
			mVal.put(col.col, (Double) val);
		} else if (val instanceof String
				&& col.type.equals(TextColumn.class)) {
			mVal.put(col.col, (String) val);
		} else if (val instanceof Float
				&& col.type.equals(FloatColumn.class)) {
			mVal.put(col.col, (Integer) val);
		} else {
			Log.w(PoiContract.TAG,
					"Skipping. Encountered val, "
							+ val
							+ ", that doesn't map to expected data-type for Column, "
							+ col + ", and expected type, " + col.type);
		}
	}

	/**
	 * Put column value from cursor into ContentValues.
	 * 
	 * @param c
	 *            Assume column value lives at the Cursor index matching
	 *            column's index
	 */
	void put(Col c, Cursor src) {
		if (src == null || src.isClosed()) {
			return;
		}
		if (c.type.equals(IntegerColumn.class)
				|| c.type.equals(BooleanColumn.class)) {
			mVal.put(c.col, src.getInt(c.ind));
		} else if (c.type.equals(DoubleColumn.class)) {
			mVal.put(c.col, src.getDouble(c.ind));
		} else if (c.type.equals(TimestampColumn.class)) {
			mVal.put(c.col, src.getLong(c.ind));
		} else if (c.type.equals(TextColumn.class)) {
			mVal.put(c.col, src.getString(c.ind));
		} else if (c.type.equals(FloatColumn.class)) {
			mVal.put(c.col, src.getFloat(c.ind));
		} else {
			Log.w(PoiContract.TAG, "Unsupported column type " + c.type);
		}
	}

}