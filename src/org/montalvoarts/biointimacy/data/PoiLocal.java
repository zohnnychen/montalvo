package org.montalvoarts.biointimacy.data;

import org.montalvoarts.biointimacy.R;

public enum PoiLocal {
	SINGLETON;
	
	public final static int RAWRES_LOCALPOI = R.raw.local_poi;
	public final static String GUID_WHATIS_BIO = "GUID_WHATIS_BIO";
}
