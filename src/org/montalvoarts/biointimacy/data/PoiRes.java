package org.montalvoarts.biointimacy.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.data.PoiResContract.Col;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.PatternMatcher;
import android.text.TextUtils;
import android.util.Log;
import edu.mit.mobile.android.content.column.BooleanColumn;
import edu.mit.mobile.android.content.column.DoubleColumn;
import edu.mit.mobile.android.content.column.FloatColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;
import edu.mit.mobile.android.content.column.TimestampColumn;

/**
 * Helper data object that encapsulates the contents of a single POI entity.
 * Values are found in the {@link #getVals()} object.
 * <p>
 * Can be intialized by a Cursor pointing to a row in the POI database.
 */
public class PoiRes {
	
	
	private static final String TAG = Util.Tag.Bio_Data.prefix("PoiRes");
	private static List<PatternMatcher> sPatterns = new ArrayList<PatternMatcher>();
	private static Set<String> sBlacklistedTypes = new HashSet<String>();
	static {
		sPatterns.add(new PatternMatcher("http://goo.gl/.*", PatternMatcher.PATTERN_SIMPLE_GLOB));
		sPatterns.add(new PatternMatcher("https://play.google.com/.*", PatternMatcher.PATTERN_SIMPLE_GLOB));
		
		sBlacklistedTypes.add("application/octet-stream");
	}

	
	// Each value's key is Col#col
	private ContentValues mVal;

	public PoiRes() {
		mVal = new ContentValues();
	}

	public boolean isValid() {
		for (Col col : Col.values()) {
			if (col.required && !mVal.containsKey(col.col)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Each resource may be associated with a remote Media URL.
	 * The domains of these Media URL may be on the list of domains
	 * that the app doesn't support or should not be rendered by the app.
	 * <p>
	 * E.g. the QRcode resources and Google Play store badge resource 
	 * 
	 * @return
	 */
	public boolean isBlacklisted() {
		
		String mediaUrl = getString(Col.MEDIA_URL);

		if (!TextUtils.isEmpty(mediaUrl)) {
			for (PatternMatcher pattern:sPatterns) {
				if (pattern.match(mediaUrl)) {
					Log.v(TAG,mediaUrl + " matched pattern " + pattern.getPath());
					return true;
				} else {
					Log.v(TAG,mediaUrl + " doesn't match pattern " + pattern.getPath());
				}
			}
		}
		
		// Check if it's a blacklisted mimetype
		String mimetype = getString(Col.MIME_TYPE);
		if (mimetype != null && sBlacklistedTypes.contains(mimetype)) {
			Log.v(TAG,"Blacklisting resource with mimetype " + mimetype);
			return true;
		}
		return false;
	}

	public String toString() {
		return mVal.toString();
	}
	
	public Intent formIntent() {
		// Always view it
		String mediaUrl = getString(Col.MEDIA_URL);
		String localUri = getString(Col.LOCAL_URI);
		String mimeType = getString(Col.MIME_TYPE);
		if (!TextUtils.isEmpty(mediaUrl)) {
			return new Intent(Intent.ACTION_VIEW, Uri.parse(mediaUrl));
		} else if (!TextUtils.isEmpty(mimeType) && !TextUtils.isEmpty(localUri)) {
			// try using just the download manager's URI
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setDataAndType(Uri.parse(localUri), mimeType);
			return i;
		}
		return null;
	}
	
	public Uri getLocalUri() {
		String local = getString(Col.LOCAL_URI);
		if (local == null) {
			return null;
		}
		return Uri.parse(local);
	}
	

	public String getString(Col col) {
		if (!col.type.equals(TextColumn.class)) {
			return null;
		}
		String val = mVal.getAsString(col.col);
		if (TextUtils.isEmpty(val)) {
			return null;
		}
		return val;
	}

	public int getInteger(Col col) {
		if (!col.type.equals(IntegerColumn.class)) {
			return -1;
		}
		Integer val = mVal.getAsInteger(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}

	public float getFloat(Col col) {
		if (!col.type.equals(FloatColumn.class)) {
			return -1;
		}
		Float val = mVal.getAsFloat(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}

	public double getDouble(Col col) {
		if (!col.type.equals(DoubleColumn.class)) {
			return -1;
		}
		Double val = mVal.getAsDouble(col.col);
		if (val == null) {
			return -1;
		}
		return val;
	}
	

	public void put(Col col, String val) {
		mVal.put(col.col, val);
	}

	public void put(Col col, int val) {
		mVal.put(col.col, val);
	}

	public void put(Col col, double val) {
		mVal.put(col.col, val);
	}

	ContentValues getVals() {
		return mVal;
	}
	
	/**
	 * Put column value from cursor into ContentValues.
	 * 
	 * @param c
	 *            Assume column value lives at the Cursor index matching
	 *            column's index
	 */
	void put(Col c, Cursor src) {
		if (src == null || src.isClosed()) {
			return;
		}
		if (c.type.equals(IntegerColumn.class)
				|| c.type.equals(BooleanColumn.class)) {
			mVal.put(c.col, src.getInt(c.ind));
		} else if (c.type.equals(DoubleColumn.class)) {
			mVal.put(c.col, src.getDouble(c.ind));
		} else if (c.type.equals(TimestampColumn.class)) {
			mVal.put(c.col, src.getLong(c.ind));
		} else if (c.type.equals(TextColumn.class)) {
			mVal.put(c.col, src.getString(c.ind));
		} else if (c.type.equals(FloatColumn.class)) {
			mVal.put(c.col, src.getFloat(c.ind));
		} else {
			Log.w(PoiContract.TAG, "Unsupported column type " + c.type);
		}
	}

}