package org.montalvoarts.biointimacy.data;

import edu.mit.mobile.android.content.ForeignKeyDBHelper;
import edu.mit.mobile.android.content.GenericDBHelper;
import edu.mit.mobile.android.content.QuerystringWrapper;
import edu.mit.mobile.android.content.SimpleContentProvider;

public class PoiProvider extends SimpleContentProvider {

	private static final int VERSION = 1;

	public PoiProvider() {
		super(Constants.AUTHORITY, VERSION);
		final GenericDBHelper poiHelper = new GenericDBHelper(PoiContract.class);
		final QuerystringWrapper poiWrapper = new QuerystringWrapper(
				poiHelper);

        // creates a relationship between BlogPosts and Comments, using Comment.POST as the column.
        // It's also responsible for creating the tables for the child.
        final ForeignKeyDBHelper foreignHelper = new ForeignKeyDBHelper(PoiContract.class, PoiResContract.class,
                PoiResContract.POI_ID);

		// This adds a mapping between the given content:// URI path and the
		// helper.
		// See {@link QuerystringWrapper} for how to form uri's
		addDirAndItemUri(poiWrapper, PoiContract.PATH);
        addChildDirAndItemUri(foreignHelper, PoiContract.PATH, PoiResContract.PATH);
        
        addDirAndItemUri(foreignHelper, PoiResContract.PATH_ALL_RESOURCES);;
	}

}
