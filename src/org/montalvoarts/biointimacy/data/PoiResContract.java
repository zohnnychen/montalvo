package org.montalvoarts.biointimacy.data;

import java.util.ArrayList;

import android.net.Uri;
import edu.mit.mobile.android.content.ContentItem;
import edu.mit.mobile.android.content.ForeignKeyDBHelper;
import edu.mit.mobile.android.content.ProviderUtils;
import edu.mit.mobile.android.content.UriPath;
import edu.mit.mobile.android.content.column.DBColumn;
import edu.mit.mobile.android.content.column.DBColumnType;
import edu.mit.mobile.android.content.column.DBForeignKeyColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;

/**
 * Abstraction of the POI's media resouces
 */

@UriPath(PoiResContract.PATH)
public class PoiResContract implements ContentItem {

	/**
	 * The table name offered by this provider
	 */
	public static final String PATH = "res";

	@DBForeignKeyColumn(parent = PoiContract.class)
	public static final String POI_ID = "poi_id";

	/*
	 * Column definitions
	 */
	@DBColumn(type = TextColumn.class, notnull = true)
	public static final String GUID = "guid";

	@DBColumn(type = TextColumn.class)
	public static final String HASH = "hash";

	@DBColumn(type = TextColumn.class)
	public static final String LOCAL_URI = "local_uri";

	@DBColumn(type = IntegerColumn.class)
	public static final String DOWNLOAD_ID = "download_id";

	@DBColumn(type = TextColumn.class)
	public static final String MEDIA_URL = "media_url";

	@DBColumn(type = TextColumn.class)
	public static final String MIME_TYPE = "mime_type";

	public static final String PATH_ALL_RESOURCES = PoiContract.PATH + "/"
			+ ForeignKeyDBHelper.WILDCARD_PATH_SEGMENT + "/" + PATH;

	public static final Uri ALL_RESOURCES = ProviderUtils.toContentUri(
			Constants.AUTHORITY, PATH_ALL_RESOURCES);

	static {
		PoiResContract.class.getFields();
	}

	public enum Col {
		ID(PoiResContract._ID, 0, IntegerColumn.class), GUID(
				PoiResContract.GUID, 1, TextColumn.class, true), HASH(
				PoiResContract.HASH, 2, TextColumn.class), LOCAL_URI(
				PoiResContract.LOCAL_URI, 3, TextColumn.class), DOWNLOAD_ID(
				PoiResContract.DOWNLOAD_ID, 4, IntegerColumn.class), MEDIA_URL(
				PoiResContract.MEDIA_URL, 5, TextColumn.class), MIME_TYPE(
				PoiResContract.MIME_TYPE, 6, TextColumn.class), POI_ID(
				PoiResContract.POI_ID, 7, IntegerColumn.class);

		Col(String colName, int index, Class<? extends DBColumnType<?>> type,
				boolean required) {
			this.col = colName;
			this.ind = index;
			this.type = type;
			this.required = required;
		}

		Col(String colName, int index, Class<? extends DBColumnType<?>> type) {
			this(colName, index, type, false);
		}

		public Class<? extends DBColumnType<?>> type;
		public String col;
		public int ind;
		public boolean required;

		/**
		 * Returns in order projection of column names
		 */
		public static String[] proj() {
			ArrayList<String> result = new ArrayList<String>();
			for (Col p : values()) {
				result.add(p.col);
			}
			return result.toArray(new String[] {});
		}

	}
}
