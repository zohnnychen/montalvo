package org.montalvoarts.biointimacy.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.montalvoarts.biointimacy.R;
import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.NoteAttributes;

import edu.mit.mobile.android.content.column.DoubleColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;

public enum PoiHelper {
	// Singleton instance of the factory
	SINGLETON;

	private static final double FEET_PER_METER = 3.2808;
	private static final float FEET_PER_MILE = 5280;
	private static final int DISTANCE_REALLY_CLOSE_FEET=60;
	private XmlPullParser mParser;


	private static final String TAG = Util.Tag.Bio_Data.prefix("Helper");

	private PoiHelper() {
		mParser = Xml.newPullParser();
		try {
			mParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		} catch (XmlPullParserException e) {
		}
	}

	/**
	 * @return null if Poi can't be instantiated from argument Uri
	 */
	public Poi create(Context context, Uri uri) {
		Cursor c = null;
		try {
			if (uri == null || !uri.getAuthority().equals(Constants.AUTHORITY)) {
				throw new IllegalArgumentException("Invalid URI " + uri);
			}
			c = context.getContentResolver().query(uri, Col.proj(), null, null,
					null);
			if (c == null || c.getCount() == 0 || !c.moveToNext()) {
				throw new IllegalStateException("No data found at uri, " + uri);
			}
			Poi poi = new Poi();
			for (Col col : Col.values()) {
				poi.put(col, c);
			}
			if (!poi.isValid()) {
				throw new IllegalStateException(
						"Poi does not contain all required columns");
			}
			return poi;
		} catch (Exception e) {
			Log.w(TAG, "Unable to create POI from uri, " + uri, e);
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}

	/**
	 * @param c
	 *            moved to a valid row in the database
	 * @return
	 */
	public Poi create(Context context, Cursor c) {
		try {
			if (c == null || c.getCount() == 0 || c.isClosed()) {
				throw new IllegalStateException("No data found");
			}
			Poi poi = new Poi();
			for (Col col : Col.values()) {
				poi.put(col, c);
			}
			if (!poi.isValid()) {
				throw new IllegalStateException(
						"Poi does not contain all required columns");
			}
			return poi;
		} catch (Exception e) {
			Log.w(TAG,
					"Unable to create POI from cursor at position, "
							+ c.getPosition(), e);
			return null;
		}
	}


	public Uri persist(Context context, Uri parentUri, PoiRes poiRes) {
		try {
			if (poiRes == null || !poiRes.isValid()) {
				throw new IllegalArgumentException("Encountered invalid Poi RES, "
						+ poiRes);
			}
			return context.getContentResolver().insert(PoiContract.RESOURCES.getUri(parentUri),
					poiRes.getVals());
		} catch (Exception e) {
			Log.w(TAG, "Unable to persist poi, " + poiRes);
			return null;
		}
	}
	
	public Uri persist(Context context, Poi poi) {
		try {
			if (poi == null || !poi.isValid()) {
				throw new IllegalArgumentException("Encountered invalid Poi, "
						+ poi);
			}
			return context.getContentResolver().insert(PoiContract.CONTENT_URI,
					poi.getVals());
		} catch (Exception e) {
			Log.w(TAG, "Unable to persist poi, " + poi);
			return null;
		}
	}

	/**
	 * Attempt to form a Poi from a JSON object that has keys equivalent to the
	 * col names found at {@link PoiContract.Col}
	 * 
	 * @return null if Poi can't be formed.
	 */
	public Poi create(JSONObject json) {
		Poi poi = new Poi();
		try {
			for (Col col : Col.values()) {
				putJsonVal(poi, col, json);
			}
			if (!poi.isValid()) {
				throw new IllegalStateException("Created poi is not valid "
						+ poi);
			}
			return poi;
		} catch (Exception e) {
			Log.w(TAG, "Unable to create POi from json " + json, e);
			return null;
		}
	}

	/**
	 * 
	 * @param note
	 * @return null if valid Poi cannot be created from argument Note
	 */
	public Poi create(Note note) {
		Poi poi = new Poi();

		for (Col col : Col.values()) {
			Object noteVal = getMappedValue(col, note);
			if (col.required && noteVal == null) {
				Log.w(TAG,
						"Cannot create Poi. No corresponding Note value set for required column, "
								+ col);
				return null;
			}
			if (noteVal != null) {
				poi.put(col, noteVal);
			}
		}
		if (!poi.isValid()) {
			Log.w(TAG, "After setting values from note " + note.getGuid()
					+ ", POI still not valid.");
			return null;
		}
		return poi;
	}

	public ContentProviderOperation createInsertOp(Poi poi) {
		Builder op = ContentProviderOperation
				.newInsert(PoiContract.CONTENT_URI);
		op.withValues(poi.getVals());
		return op.build();
	}

	public ContentProviderOperation createUpdateOp(Poi poi) {
		Uri uri = formUriFromNoteGuid(poi.getString(Col.GUID));
		Builder op = ContentProviderOperation.newUpdate(uri);
		op.withValues(poi.getVals());
		return op.build();
	}

	public boolean delete(Context context, String noteGuid) {
		Uri uri = formUriFromNoteGuid(noteGuid);
		return context.getContentResolver().delete(uri, null, null) == 1;
	}

	/**
	 * Convert to approximate human-readable representation of the distance in
	 * English units (e.g. feet and miles)
	 * 
	 * @param prox
	 * @return
	 */
	public String prettyEnglishDistance(float prox, Context context) {
		Resources res = context.getResources();
		int feet = convertMetersToFeet(prox);
		if (feet <= DISTANCE_REALLY_CLOSE_FEET) {  
			return res.getString(R.string.dist_really_close);
		} else if (feet > DISTANCE_REALLY_CLOSE_FEET && feet < 500) { // ~ 0.1 miles 
			return res.getQuantityString(R.plurals.dist_feet, feet, feet);
		} else {
			float miles = convertFeetToMiles(feet);
			if (miles == 1) {
				return res.getString(R.string.dist_miles_one, miles);
			} else {
				return res.getString(R.string.dist_miles_other, miles);
			}
		}
		
	}

	/**
	 * 
	 * @param enml
	 *            Expected to be in Evernote ENML vo
	 * @param callback
	 *            override serializing behavior
	 * @return null if cannot be converted
	 * @return enml converted to html, with overriden behavior
	 */
	public Map<String, Uri> extractResHash2Uri(String enml) {
		Map<String,Uri> hash2Intent = new HashMap<String,Uri>();
		StringReader in = new StringReader(enml);
		try {
			mParser.setInput(in);
			String currentUrl = null;
			String currentHash = null;
			
			while (mParser.next() != XmlPullParser.END_DOCUMENT) {
				int type = mParser.getEventType();

				switch (type) {
				case XmlPullParser.START_TAG:
					if (mParser.getName().equalsIgnoreCase("a")) {
						currentUrl = mParser.getAttributeValue(null, "href");
					} else if (mParser.getName().equalsIgnoreCase("en-media")) {
						currentHash = mParser.getAttributeValue(null, "hash");
					}
					break;
				case XmlPullParser.END_TAG:
					if (mParser.getName().equalsIgnoreCase("a")) {
						if (!TextUtils.isEmpty(currentHash) && !TextUtils.isEmpty(currentUrl)) {
							hash2Intent.put(currentHash, Uri.parse(currentUrl));
						}
						currentUrl = null;
						currentHash = null;
					} 					
					break;
				}
			}
			Log.v(TAG,"Created hash2Intent mapping " + hash2Intent);
		} catch (Exception e) {
			Log.w(TAG, "Unable to convert enml to html", e);
		} finally {
			in.close();
		}
		return hash2Intent;
	}

	public Uri formUriFromNoteGuid(String noteGuid) {
		return PoiContract.CONTENT_URI.buildUpon()
				.appendQueryParameter(Col.GUID.col, noteGuid).build();
	}

	/**
	 * @return null if could not be inserted
	 */
	public Uri insert(Context context, Poi poi) {
		if (!poi.isValid()) {
			return null;
		}
		return context.getContentResolver().insert(PoiContract.CONTENT_URI,
				poi.getVals());
	}

	/**
	 * @param uri
	 *            Uri pointing to a PoiContract
	 * @return Whether the entity already exists in database or not
	 */
	public boolean exists(Context context, Uri uri) {
		Cursor c = null;
		try {
			c = context.getContentResolver().query(uri, Col.proj(), null, null,
					null);
			return (c != null && c.getCount() == 1);
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}

	public Bitmap decodeSampledBitmap(Context context, Uri uri, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();

		InputStream in = null;
		try {
			in = context.getContentResolver().openInputStream(uri);
			Log.v(TAG, "Trying to decode with uri " + uri);
			//
			// // Just get some meta info first
			// options.inJustDecodeBounds = true;
			// BitmapFactory.decodeStream(in, null, options);
			//
			// // Calculate inSampleSize
			// options.inSampleSize = calculateInSampleSize(options, reqWidth,
			// reqHeight);
			//
			// // Decode bitmap with inSampleSize set
			// TODO don't assume downsample rate
			options.inSampleSize = 2;
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeStream(in, null, options);
		} catch (FileNotFoundException e) {
			Log.w(TAG,
					"Unable to decode to bitmap: " + uri + ", msg: "
							+ e.getMessage());
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Log.w(TAG, "Unable to close bitmap stream", e);
				}
			}
		}
	}

	public List<PoiRes> listResources(Context context, long poiRowId) {
		Uri poiUri = ContentUris.withAppendedId(PoiContract.CONTENT_URI, poiRowId);
		Uri res = PoiContract.RESOURCES.getUri(poiUri);

		Cursor c = null;
		List<PoiRes> result = new ArrayList<PoiRes>();
		try {
			c = context.getContentResolver().query(res, PoiResContract.Col.proj(), null, null, null);
			while (c!=null && c.moveToNext()) {
				PoiRes poiRes = createRes(context, c);
				if (poiRes != null && poiRes.isValid()){
					result.add(poiRes);
				}
			}
			return result;
		} finally {
			if (c!=null) {
				c.close();
			}
		}
	}

	public PoiRes createRes(Context context, Cursor c) {
		try {
			if (c == null || c.getCount() == 0 || c.isClosed()) {
				throw new IllegalStateException("No data found");
			}
			PoiRes poi = new PoiRes();
			for (PoiResContract.Col col : PoiResContract.Col.values()) {
				poi.put(col, c);
			}
			if (!poi.isValid()) {
				throw new IllegalStateException(
						"PoiRes does not contain all required columns");
			}
			return poi;
		} catch (Exception e) {
			Log.w(TAG,
					"Unable to create POI Res from cursor at position, "
							+ c.getPosition(), e);
			return null;
		}
	}

	/**
	 * @return typed-value from the note matching the Column. E.g. a String for
	 *         the Note Title.
	 * @return null if no value can be found, or it's a Column not supported by
	 *         the note type
	 */
	private Object getMappedValue(Col col, Note note) {
		NoteAttributes attr = note.getAttributes();
		Object val = null;
		switch (col) {
		case GUID:
			if (note.isSetGuid()) {
				val = note.getGuid();
			}
			break;
		case TITLE:
			if (note.isSetTitle()) {
				val = note.getTitle();
			}
			break;
		case LAT:
			if (attr.isSetLatitude()) {
				val = attr.getLatitude();
			}
			break;
		case LONG:
			if (attr.isSetLongitude()) {
				val = attr.getLongitude();
			}
			break;
		case NOTE_USN:
			if (note.isSetUpdateSequenceNum()) {
				val = note.getUpdateSequenceNum();
			}
			break;
		default:
			// maybe be a null value
			break;
		}
		return val;
	}

	@SuppressWarnings("unused")
	private int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	private void putJsonVal(Poi poi, Col col, JSONObject json)
			throws JSONException {
		if (!json.has(col.col)) {
			return;
		}
		if (col.type.equals(TextColumn.class)) {
			poi.put(col, json.getString(col.col));
		} else if (col.type.equals(IntegerColumn.class)) {
			poi.put(col, json.getInt(col.col));
		} else if (col.type.equals(DoubleColumn.class)) {
			poi.put(col, json.getDouble(col.col));
		} else {
			Log.w(TAG, "Encountered unsupported type, " + col.type);
		}
	}

	private int convertMetersToFeet(double prox) {
		// function converts Feet to Meters.
		double toFeet = prox;
		toFeet = prox * FEET_PER_METER; // official conversion rate of Meters to Feet
		String formattedNumber = new DecimalFormat("0").format(toFeet);
		return Integer.valueOf(formattedNumber.trim()).intValue();
	}

	private float convertFeetToMiles(int feet) {
		// function converts Feet to Meters.
		float miles = (float)feet/FEET_PER_MILE;
		
		String formattedNumber = new DecimalFormat("0.0").format(miles);
		return Float.valueOf(formattedNumber.trim()).floatValue();
	}
}
