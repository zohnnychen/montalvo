package org.montalvoarts.biointimacy.data;

public class Constants {

	/**
	 * Account type string.
	 */
	public static final String ACCOUNT_TYPE = "org.montalvoarts";
	public static final String ACCOUNT_NAME = "org.montalvoarts.guest";

	/**
	 * If auth fails, notify user and give up on sync attempt
	 */
	public static final boolean NOTIFY_AUTH_FAILURE = true;

	/**
	 * Content provider AUTHORITY and specifies the data that the SyncAdapter
	 * will sync
	 */
	public static final String AUTHORITY = "org.montalvoarts.poi";
	public static final int SYNC_POLL_PERIOD_SECS = 60 * 60 * 4; // every 4
																	// hours

}
