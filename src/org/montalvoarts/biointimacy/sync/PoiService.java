package org.montalvoarts.biointimacy.sync;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.montalvoarts.biointimacy.Analytics.Event;
import org.montalvoarts.biointimacy.Analytics.Timing;
import org.montalvoarts.biointimacy.R;
import org.montalvoarts.biointimacy.Util;
import org.montalvoarts.biointimacy.data.Constants;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiRes;
import org.montalvoarts.biointimacy.data.PoiResContract;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.evernote.edam.notestore.NoteStore.Client;
import com.evernote.edam.notestore.SyncChunk;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;
import com.google.analytics.tracking.android.EasyTracker;

public class PoiService extends IntentService {

	// EXTRA for {@link Cmd#SYNC}
	public static final String EXTRA_STR_THUMBURI = "EXTRA_STR_THUMBURI";
	public static final String EXTRA_LONG_DOWNLOADID = "EXTRA_STR_DOWNLOADID";
	public static final String EXTRA_STR_POIURI = "EXTRA_STR_POIURI";

	private static final String NAME = "PoiSvc";
	private static final String TAG = Util.Tag.Bio_Sync.prefix("Svc");

	public static final String ACTION_SYNCING = "ACTION_SYNCING";
	public static final String EXTRA_SYNCING_ISACTIVE = "EXTRA_ISACTIVE";
	public static final String EXTRA_SYNCING_LOCATION = "EXTRA_LOCATION";

	public static final String ACTION_UPDATED_THUMBNAILS = "ACTION_UPDATED_URI";
	public static final String EXTRA_STRING_OWNERGUID = "EXTRA_URI_THUMBOWNER";
	public static final String EXTRA_URI_LOCALTHUMB = "EXTRA_URI_LOCALTHUMB";

	public static final String ACTION_UPDATED_DISTANCES = "ACTION_UPDATED_DISTANCES";
	public static final String EXTRA_URI_CLOSESTPOI = "EXTRA_URI_CLOSESTPOI";

	public static final String MIMETYPE_POI = "vnd.android.cursor.item/vnd.org.montalvoarts.poi.poicontract";
	public static final String MIMETYPE_POIRES = "vnd.android.cursor.item/vnd.org.montalvoarts.poi.poirescontract";

	/**
	 * If you're within this distance of a POI, then you are considered "close"
	 * to it
	 */
	private static final float MIN_METERS_CONSIDERED_CLOSE = 15.0f;
	private static final long WEEK_MILLIS = 60000 * 60 * 24 * 7;

	private EvernoteClient mEvernote;
	// PoiService is re-instantiated everytime a new intent sent. So
	// keep this in static state
	private static HashMap<Long, Uri> sDownloadId2Uri = new HashMap<Long, Uri>();

	public PoiService() {
		super(NAME);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mEvernote = new EvernoteClient(this);
		EasyTracker.getInstance().setContext(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Cmd cmd = Cmd.resolve(intent);
		if (cmd == null) {
			Log.w(TAG,
					"Unable to resolve intent to a known command, "
							+ intent.getAction());
			return;
		}
		Log.d(TAG, "Handling command, " + cmd + ", with intent " + intent);
		long startTime = System.currentTimeMillis();
		try {
			switch (cmd) {
			case POI_SYNC_IF_NEEDED:
				long lastSync = mEvernote.getLastSyncMillis();
				if (lastSync <= 0) {
					Log.i(TAG, "Syncing the very first time");
					startService(PoiService.Cmd.POI_CLEAN_SYNC.formIntent(this));
					EasyTracker.getInstance().setContext(this);
					EasyTracker.getTracker().sendEvent(
							Event.FIRST_TIME_LAUNCH_APP.category,
							Event.FIRST_TIME_LAUNCH_APP.name(), null, null);
				} else {
					long now = System.currentTimeMillis();
					if ((now - lastSync) > WEEK_MILLIS) {
						Log.i(TAG,
								"It's been more than a week since last sync. Syncing");
						startService(PoiService.Cmd.POI_SYNC.formIntent(this));
					}
				}
				break;
			case POI_SYNC:
				handleSync();
				EasyTracker.getTracker().sendTiming(
						Timing.SYNC_FINISH.category,
						System.currentTimeMillis() - startTime,
						Timing.SYNC_FINISH.name(), null);
				break;
			case POI_CLEAN_SYNC:
				// Reset Evernote so that client state is cleared
				mEvernote.reset();

				// Remove all files in the resource directory
				cleanAndCreateDir();

				// Clean up db
				int numDeleted = getContentResolver().delete(
						PoiContract.CONTENT_URI, null, null);
				Log.i(TAG, "Handling clean sync. Deleted " + numDeleted
						+ " old entries");

				handleSync();
				EasyTracker.getTracker().sendTiming(
						Timing.CLEAN_SYNC_FINISH.category,
						System.currentTimeMillis() - startTime,
						Timing.CLEAN_SYNC_FINISH.name(), null);
				break;
			case POI_UPDATE_NOTE:
				Client client = mEvernote.connect();
				String poiUri = intent.getStringExtra(EXTRA_STR_POIURI);
				handleUpdateBody(client, poiUri);
				handleUpdateNoteUrl(client, poiUri);
				break;
			case POI_UPDATE_THUMBURI:
				handleUpdateThumb(intent.getStringExtra(EXTRA_STR_THUMBURI),
						intent.getLongExtra(EXTRA_LONG_DOWNLOADID, -1));
				break;
			case POI_UPDATE_DISTANCES:
				Parcelable p = intent
						.getParcelableExtra(EXTRA_SYNCING_LOCATION);
				if (p != null && p instanceof Location) {
					handleUpdateDistances((Location) p);
				} else {
					Log.w(TAG,
							"Missing expected location parameter for intent, "
									+ intent);
				}
				break;
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to handle command, " + cmd, e);
		}

	}

	private void cleanAndCreateDir() {
		cancelDownloads(PoiResContract.ALL_RESOURCES,
				PoiResContract.Col.proj(), PoiResContract.DOWNLOAD_ID + ">0",
				PoiResContract.Col.DOWNLOAD_ID.ind);
		cancelDownloads(PoiContract.CONTENT_URI, PoiContract.Col.proj(),
				PoiContract.Col.THUMB_DWNLD_ID + ">0",
				PoiContract.Col.THUMB_DWNLD_ID.ind);

		File path = Environment
				.getExternalStoragePublicDirectory(Util.EXT_DIR_NAME);
		Log.d(TAG, "Does resource dir already exist? "
				+ (path.isDirectory() && path.exists()));
		if (path.exists()) {
			for (File f : path.listFiles()) {
				if (!f.delete()) {
					Log.w(TAG, "Unable to delete file " + f);
				}
			}
		} else {
			if (!path.mkdir()) {
				Log.i(TAG, "Unable to create dir " + path);
			}
		}
	}

	private void cancelDownloads(Uri uri, String[] proj, String selection,
			int ind) {
		DownloadManager downloader = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		if (downloader == null) {
			return;
		}
		// Cancel all possible downloads
		Cursor c = null;
		int count = 0;
		try {
			c = getContentResolver().query(uri, proj, selection, null, null);
			while (c.moveToNext()) {
				downloader.remove(c.getLong(ind));
				count++;
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to cancel download", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		Log.d(TAG, "Attempted to cancel " + count + " downloads");
	}

	private void handleUpdateNoteUrl(Client client, String poiUriStr) {
		if (TextUtils.isEmpty(poiUriStr)) {
			Log.w(TAG, "No poi uri set");
			return;
		}
		Uri poiUri = Uri.parse(poiUriStr);
		Poi poi = PoiHelper.SINGLETON.create(this, poiUri);
		if (poi == null) {
			Log.w(TAG, "Unable to get poi with uri " + poiUri);
			return;
		}
		String noteGuid = poi.getString(PoiContract.Col.GUID);
		if (TextUtils.isEmpty(noteGuid)) {
			Log.w(TAG, "No note guid associated with uri " + poiUriStr);
			return;
		}
		String noteKey = mEvernote.shareNote(client, noteGuid);
		if (noteKey == null) {
			Log.w(TAG, "Unable to share note. ABorting");
			return;
		}

		Uri publicUri = mEvernote.formNotePublicUrl(noteGuid, noteKey);
		if (publicUri == null) {
			Log.w(TAG, "Unable to form public uri");
		}
		Log.v(TAG, "Formed public URI " + publicUri);
		ContentValues val = new ContentValues();
		val.put(PoiContract.Col.PUBLIC_URI.col, publicUri.toString());
		if (getContentResolver().update(poiUri, val, null, null) != 1) {
			Log.w(TAG, "Unable to update poi with its public url " + publicUri);
		}

	}

	private void handleSync() {
		Intent active = new Intent(ACTION_SYNCING);
		active.putExtra(EXTRA_SYNCING_ISACTIVE, true);

		// Report progress
		sendBroadcast(active);

		// sync local
		handleLocalSync();

		// sync remote
		Client client = mEvernote.connect();
		handleRemoteSync(client);

		// update mapping of downloadId to URI's
		updateDownloadId2Uri(sDownloadId2Uri);

		active.putExtra(EXTRA_SYNCING_ISACTIVE, false);
		sendBroadcast(active);
	}

	private void updateDownloadId2Uri(HashMap<Long, Uri> downloadId2Uri) {
		// Clear out the current mappings
		downloadId2Uri.clear();

		Cursor poiC = null;
		Cursor resC = null;
		try {
			poiC = getContentResolver().query(
					PoiContract.CONTENT_URI,
					PoiContract.Col.proj(),
					PoiContract.Col.THUMB_DWNLD_ID + ">0 and "
							+ PoiContract.Col.THUMB_URI + " is null", null,
					null);
			while (poiC != null && poiC.moveToNext()) {
				long id = poiC.getLong(PoiContract.Col.ID.ind);
				long downloadId = poiC
						.getLong(PoiContract.Col.THUMB_DWNLD_ID.ind);
				if (downloadId > 0 && id > 0) {
					downloadId2Uri.put(downloadId, ContentUris.withAppendedId(
							PoiContract.CONTENT_URI, id));
				}
			}
			resC = getContentResolver().query(
					PoiResContract.ALL_RESOURCES,
					PoiResContract.Col.proj(),
					PoiResContract.Col.DOWNLOAD_ID + ">0 and "
							+ PoiResContract.Col.LOCAL_URI + " is null", null,
					null);
			while (resC != null && resC.moveToNext()) {
				long poiResId = resC.getLong(PoiResContract.Col.ID.ind);
				long downloadId = resC
						.getLong(PoiResContract.Col.DOWNLOAD_ID.ind);
				long poiId = resC.getLong(PoiResContract.Col.POI_ID.ind);
				Uri poiUri = ContentUris.withAppendedId(
						PoiContract.CONTENT_URI, poiId);
				Uri poiResUri = PoiContract.RESOURCES.getUri(poiUri, poiResId);
				if (downloadId > 0 && poiId > 0 && poiResId > 0) {
					downloadId2Uri.put(downloadId, poiResUri);
				}
			}
			Log.v(TAG, "Out of " + (poiC.getCount() + resC.getCount())
					+ " poi and poi resources, got " + downloadId2Uri.size()
					+ " downloadId->URI mappings");
		} finally {
			if (poiC != null) {
				poiC.close();
			}
			if (resC != null) {
				resC.close();
			}
		}

	}

	private void handleUpdateDistances(Location current) {
		// Get current distance
		if (!current.hasAccuracy()) {
			Log.w(TAG, "Unable to get current location. Aborting.");
			return;
		}
		Log.d(TAG,
				"Current location is (accuracy/lat/long): "
						+ current.getAccuracy() + " / " + current);
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		Cursor c = null;
		try {
			float smallestDistance = Float.MAX_VALUE;
			Uri closestPoi = null;
			c = getContentResolver().query(PoiContract.CONTENT_URI, Col.proj(),
					null, null, null);
			while (c != null && c.moveToNext()) {
				String guid = c.getString(Col.GUID.ind);
				Uri uri = ContentUris.withAppendedId(PoiContract.CONTENT_URI,
						c.getLong(Col.ID.ind));

				double longitude = c.getDouble(Col.LONG.ind);
				double latitude = c.getDouble(Col.LAT.ind);
				if (longitude == 0 && latitude == 0) {
					Log.v(TAG,
							"Missing required lat and long, skipping "
									+ c.getString(PoiContract.Col.TITLE.ind));
					continue;
				}
				// Calculate proximity
				Location poiLoc = new Location(current);
				poiLoc.setLatitude(latitude);
				poiLoc.setLongitude(longitude);

				float distance = current.distanceTo(poiLoc);
				if (distance < smallestDistance) {
					smallestDistance = distance;
					closestPoi = uri;
				}
				Log.v(TAG, "Found that current location is " + distance
						+ " meters from POI, " + guid);

				Poi poi = new Poi();
				poi.put(Col.GUID, guid);
				poi.put(Col.PROXIMITY_METERS, distance);
				ops.add(PoiHelper.SINGLETON.createUpdateOp(poi));
			}
			Log.d(TAG, "Found closest guid to be " + closestPoi
					+ " at distance " + smallestDistance);
			if (closestPoi != null && smallestDistance > 0
					&& smallestDistance < MIN_METERS_CONSIDERED_CLOSE) {
				Intent i = new Intent(ACTION_UPDATED_DISTANCES);
				i.putExtra(EXTRA_URI_CLOSESTPOI, closestPoi);
				sendBroadcast(i);
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}

		if (ops.size() > 0) {
			ContentProviderResult[] result;
			try {
				result = getContentResolver().applyBatch(Constants.AUTHORITY,
						ops);
				Log.d(TAG,
						"Updating "
								+ ops.size()
								+ " poi's proximities to current location and got num results, "
								+ result.length);
			} catch (Exception e) {
				Log.w(TAG, "Unable to update proximities", e);
			}

		} else {
			Log.i(TAG, "No promixities to update");
		}
	}

	private void handleLocalSync() {
		try {
			String localPoiStr = getStringFromRes(R.raw.local_poi);
			if (TextUtils.isEmpty(localPoiStr)) {
				Log.w(TAG, "Unable to get raw res for local poi");
				return;
			}
			JSONArray poiArray = new JSONArray(localPoiStr);
			int size = poiArray.length();
			ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
			for (int i = 0; i < size; i++) {
				JSONObject singleLocal = poiArray.getJSONObject(i);
				Poi poi = PoiHelper.SINGLETON.create(singleLocal);
				if (poi == null || !poi.isValid()) {
					Log.w(TAG, "Unable to serialize Poi " + poi
							+ ", from JSON " + singleLocal);
					continue;
				}

				// If already exists, skip it
				String noteGuid = poi.getString(Col.GUID);
				if (PoiHelper.SINGLETON.exists(this,
						PoiHelper.SINGLETON.formUriFromNoteGuid(noteGuid))) {
					Log.d(TAG, "Skipping local poi that already exists, "
							+ noteGuid);
					continue;
				}
				// Assume a local URI
				String thumbUri = poi.getString(Col.THUMB_URI);
				if (!TextUtils.isEmpty(thumbUri)) {
					sendThumbUpdatedBroadcast(noteGuid, thumbUri);
				}

				ops.add(PoiHelper.SINGLETON.createInsertOp(poi));
			}
			if (ops.size() > 0) {
				insertBatch(ops);
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to load local POI", e);
		}

	}

	private void sendThumbUpdatedBroadcast(String noteGuid, String thumbUri) {
		Intent i = new Intent(PoiService.ACTION_UPDATED_THUMBNAILS);
		i.putExtra(PoiService.EXTRA_STRING_OWNERGUID, noteGuid);
		i.putExtra(PoiService.EXTRA_URI_LOCALTHUMB, Uri.parse(thumbUri));
		sendBroadcast(i);
	}

	private void handleUpdateBody(Client client, String poiUriStr) {

		// Get note guid
		Cursor c = null;
		try {
			Uri poiUri = Uri.parse(poiUriStr); // will throw NPE if necessary
			c = getContentResolver()
					.query(poiUri, Col.proj(), null, null, null);
			if (c == null || c.getCount() == 0 || !c.moveToNext()) {
				throw new Exception("Unable to find POI matching uri, "
						+ poiUri);
			}
			String noteGuid = c.getString(Col.GUID.ind);
			Note note = mEvernote.getNote(client, noteGuid);
			Log.v(TAG, "Attempting to get body for note with guid " + noteGuid
					+ " and poi URI " + poiUriStr);
			if (note.getContentLength() > 0) {
				String content = note.getContent();
				ContentValues val = new ContentValues();
				val.put(Col.BODY.col, content);

				// Update DB
				if (getContentResolver().update(poiUri, val, null, null) != 1) {
					throw new Exception(
							"Unable to update POI with note body of size, "
									+ note.getContentLength());
				}
				Log.d(TAG, "Successfully updated POI with URI " + poiUriStr
						+ " with body of size " + note.getContentLength());

				// Parse the ENML body and extract the URL link associated with
				// each
				// en-media resource embedded in the ENML. For example, the
				// picture of a youtube thumbnail
				// will be associated with a URL to a youtube video.

				// Hash string -> Uri
				Map<String, Uri> mediaHash2Uri = PoiHelper.SINGLETON
						.extractResHash2Uri(content);
				updateMediaUri(poiUri, mediaHash2Uri);

			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to update POI body content for POI with uri "
					+ poiUriStr, e);
		}
	}

	/**
	 * Db operation to update all the resources
	 * 
	 * @param poiUri
	 * @param mediaHash2Uri
	 */
	private void updateMediaUri(Uri poiUri, Map<String, Uri> mediaHash2Uri) {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		Cursor c = null;
		try {
			c = getContentResolver().query(
					PoiContract.RESOURCES.getUri(poiUri),
					PoiResContract.Col.proj(), null, null, null);
			while (c != null && c.moveToNext()) {
				String hash = c.getString(PoiResContract.Col.HASH.ind);
				Uri mediaUri = mediaHash2Uri.get(hash);
				if (mediaUri != null) {
					Uri resUri = PoiContract.RESOURCES.getUri(poiUri,
							c.getLong(PoiResContract.Col.ID.ind));
					ContentProviderOperation.Builder b = ContentProviderOperation
							.newUpdate(resUri);
					b.withValue(PoiResContract.Col.MEDIA_URL.col,
							mediaUri.toString());
					ops.add(b.build());
				}
			}
			ContentProviderResult[] result = getContentResolver().applyBatch(
					Constants.AUTHORITY, ops);
			Log.v(TAG, "Out of " + mediaHash2Uri.size()
					+ " mappings, successfully updated " + result.length
					+ " out of " + ops.size() + " poi-res update attempts");
		} catch (Exception e) {
			Log.w(TAG, "Unable to update media url for poi " + poiUri, e);
		} finally {
			if (c != null) {
				c.close();
			}
		}

	}

	private static DateFormat sFormat = DateFormat.getDateTimeInstance();

	private void handleUpdateThumb(String thumbUri, long downloadId) {
		if (TextUtils.isEmpty(thumbUri) || downloadId <= 0) {
			Log.w(TAG, "Extra values are invalid. Won't update thumbnail uri");
		}
		// Update POI that matches this downloadId
		try {
			if (sDownloadId2Uri.isEmpty()) {
				Log.w(TAG, "Can't handle. No mapping of downloadId's to URI's");
				return;
			}
			Uri impacted = sDownloadId2Uri.get(downloadId);
			if (impacted == null) {
				Log.w(TAG, "No mapping found for downloadId " + downloadId
						+ ". Aborting");
				return;
			}
			String resolvedType = getContentResolver().getType(impacted);
			if (resolvedType == null) {
				Log.w(TAG, "Unable to resolve URi to a known type " + impacted);
				return;
			}
			ContentValues val = new ContentValues();
			if (resolvedType.equals(MIMETYPE_POI)) {
				val.put(PoiContract.Col.THUMB_URI.col, thumbUri);
				val.putNull(PoiContract.Col.THUMB_DWNLD_ID.col);
			} else if (resolvedType.equals(MIMETYPE_POIRES)) {
				val.put(PoiResContract.Col.LOCAL_URI.col, thumbUri);
				val.putNull(PoiResContract.Col.DOWNLOAD_ID.col);
			} else {
				Log.w(TAG, "Unrecognized mimetype for URI " + impacted
						+ ". Aborting");
				return;
			}

			if (getContentResolver().update(impacted, val, null, null) != 1) {
				Log.w(TAG,
						"Unable to update impacted URI with new thumbUri value, "
								+ thumbUri);
			} else {
				Log.d(TAG, "For downloadId, " + downloadId
						+ ", updated poi URI " + impacted
						+ " with local media uri " + thumbUri);

				String guid = queryForGuid(resolvedType, impacted);
				if (TextUtils.isEmpty(guid)) {
					Log.w(TAG, "Unable to get guid from URI " + impacted
							+ " and resolved type " + resolvedType);
				} else {
					sendThumbUpdatedBroadcast(guid, thumbUri);
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to update URI that match downloadID "
					+ downloadId, e);
		}
	}

	private String queryForGuid(String resolvedType, Uri impacted) {
		Cursor c = null;
		String guid = null;
		String[] proj = null;
		int index = -1;
		try {
			if (resolvedType.equals(MIMETYPE_POI)) {
				proj = PoiContract.Col.proj();
				index = PoiContract.Col.GUID.ind;

			} else if (resolvedType.equals(MIMETYPE_POIRES)) {
				proj = PoiResContract.Col.proj();
				index = PoiResContract.Col.GUID.ind;
			}
			c = getContentResolver().query(impacted, proj, null, null, null);
			if (c.moveToNext()) {
				guid = c.getString(index);
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return guid;
	}

	private void handleRemoteSync(Client client) {
		if (client == null) {
			Log.w(TAG, "Invalid client. Skipping sync");
			return;
		}

		if (!mEvernote.checkAndSetLastUsn(client)) {
			Log.w(TAG, "Unable to check sync status. Aborting sync");
			return;
		}

		SyncChunk chunk = mEvernote.getSyncChunk(client);
		if (chunk == null || !chunk.isSetNotes()) {
			Log.w(TAG, "Unable to obtain sync chunk. Aborting sync");
			return;
		}

		Set<Uri> insertedPoi = syncNotes(chunk);

		for (Uri inserted : insertedPoi) {
			Intent i = Cmd.POI_UPDATE_NOTE.formIntent(this);
			i.putExtra(EXTRA_STR_POIURI, inserted.toString());
			startService(i);
		}

		Log.i(TAG,
				"Given sync-chunk with " + chunk.getNotesSize()
						+ " notes and chunk's highest USN at "
						+ chunk.getChunkHighUSN() + ", inserted "
						+ insertedPoi.size()
						+ " POI entities in db. \n\nClient's high USN is "
						+ mEvernote.getLastUsn() + " and last client sync at "
						+ sFormat.format(mEvernote.getLastSyncMillis())
						+ ". Requested that " + insertedPoi.size()
						+ " note content be downloaded");
	}

	/**
	 * @return URI's of POI rows inserted. May be the empty set
	 */
	private Set<Uri> syncNotes(SyncChunk chunk) {
		Map<String, Integer> existing = getPoiGuid2Usn();
		PoiHelper helper = PoiHelper.SINGLETON;

		Iterator<Note> it = chunk.getNotesIterator();
		Set<Uri> inserted = new HashSet<Uri>();
		Log.d(TAG, "Syncing " + chunk.getNotesSize() + " notes");
		while (it.hasNext()) {
			Note note = it.next();
			if (note.isSetDeleted() && note.getDeleted() > 0) {
				if (note.isSetGuid() && existing.containsKey(note.getGuid())) {
					Log.d(TAG,
							"Deleting a POI that has been marked deleted on the server, "
									+ note.getGuid());
					helper.delete(this, note.getGuid());
				} else {
					Log.d(TAG, "Skipping deleted note, " + note.getGuid());
				}
				continue;
			}
			Poi poi = helper.create(note);
			if (poi == null) {
				Log.w(TAG, "Skipping. Unable to form valid POI from note, "
						+ note.getGuid());
				continue;
			}

			String candidateGuid = poi.getString(Col.GUID);
			if (existing.containsKey(candidateGuid)
					&& note.getUpdateSequenceNum() == existing
							.get(candidateGuid)) {
				// If it's an unchanged note, do nothing
				Log.v(TAG, "Skipping an unchanged note, " + candidateGuid);
				continue;
			}

			// At this point, note is either brand new or has been updated

			// Request download of this note's thumbnail
			long downloadId = mEvernote.downloadNoteThumbnail(
					poi.getString(Col.GUID), Util.EXT_DIR_NAME);
			if (downloadId > 0) {
				// Update POI with new download ID
				poi.put(Col.THUMB_DWNLD_ID, downloadId);
			}

			if (existing.containsKey(candidateGuid)
					&& note.getUpdateSequenceNum() > existing
							.get(candidateGuid)) {
				Log.d(TAG,
						"Deleting a POI that needs to be updated. It's USN is "
								+ note.getUpdateSequenceNum());
				helper.delete(this, candidateGuid);
			}
			// Then add back any new POI or one that we just deleted
			Uri poiUri = helper.persist(this, poi);
			inserted.add(poiUri);

			// add the resources
			if (note.getResourcesSize() > 0) {
				int numInserted = insertResourcesAndDownload(poiUri,
						note.getResources());
				Log.d(TAG, "For note, " + poi.getString(Col.TITLE)
						+ ", inserted " + numInserted + " resources out of "
						+ note.getResourcesSize());
			}

		}
		return inserted;
	}

	/**
	 * @return number of resources persisted and downloads requested
	 */
	private int insertResourcesAndDownload(Uri poiUri, List<Resource> resources) {
		int numInserted = 0;
		for (Resource res : resources) {
			if (res.isSetGuid() && res.isSetData()) {
				byte[] hash = res.getData().getBodyHash();
				String resGuid = res.getGuid();
				String mimeType = res.getMime();
				if (!TextUtils.isEmpty(mimeType) && !TextUtils.isEmpty(resGuid)
						&& hash != null && hash.length > 0) {
					PoiRes poiRes = new PoiRes();
					poiRes.put(PoiResContract.Col.GUID, resGuid);
					poiRes.put(PoiResContract.Col.HASH, byteArrayToHex(hash));
					poiRes.put(PoiResContract.Col.MIME_TYPE, mimeType);

					if (poiRes.isBlacklisted()) {
						continue;
					}

					// Request download of this resources thumbnail
					long downloadId = mEvernote.downloadResThumbnail(resGuid,
							Util.EXT_DIR_NAME);
					if (downloadId > 0) {
						// Update POI with new download ID
						poiRes.put(PoiResContract.Col.DOWNLOAD_ID, downloadId);
					}

					// insert it
					if (PoiHelper.SINGLETON.persist(this, poiUri, poiRes) == null) {
						Log.w(TAG, "Unable to insert res with guid " + resGuid);
					} else {
						numInserted++;
					}

				}
			}
		}
		return numInserted;
	}

	private Map<String, Integer> getPoiGuid2Usn() {
		Map<String, Integer> result = new HashMap<String, Integer>();
		Cursor c = null;
		try {
			c = getContentResolver().query(PoiContract.CONTENT_URI, Col.proj(),
					null, null, null);
			while (c != null && c.moveToNext()) {
				String guid = c.getString(Col.GUID.ind);
				int usn = c.getInt(Col.NOTE_USN.ind);
				if (!TextUtils.isEmpty(guid) && usn > 0) {
					result.put(guid, usn);
				}
			}
			return result;
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}

	private String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder();
		for (byte b : a) {
			sb.append(String.format("%02x", b & 0xff));
		}
		return sb.toString();
	}

	/**
	 * @return Set of URI representing POI rows that were successfully inserted.
	 *         May be an empty set.
	 */
	private Set<Uri> insertBatch(ArrayList<ContentProviderOperation> ops) {
		Set<Uri> result = new HashSet<Uri>();
		try {
			ContentProviderResult[] results = getContentResolver().applyBatch(
					Constants.AUTHORITY, ops);
			Log.i(TAG, "Successfully executed " + results.length
					+ " out of possible " + ops.size() + " db operations");
			for (ContentProviderResult r : results) {
				if (r.uri != null) {
					result.add(r.uri);
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to batch insert " + ops.size() + " POI", e);
		}
		return result;

	}

	/**
	 * @param rawRes
	 *            one fo R.raw.foobar
	 * @return null if resource can't be serialized to String
	 */
	private String getStringFromRes(int rawRes) {
		InputStream inputStream = null;
		try {
			inputStream = getResources().openRawResource(rawRes);
			// json is UTF-8 by default
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			return sb.toString();
		} catch (Exception e) {
			Log.w(TAG, "Unable to read res with id " + rawRes, e);
			return null;
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (Exception squish) {
			}
		}
	}

	/**
	 * Possible commands to the service. Intent's action name is the canonical
	 * name of the enum
	 */
	public enum Cmd {
		POI_SYNC, POI_UPDATE_THUMBURI(EXTRA_STR_THUMBURI, EXTRA_LONG_DOWNLOADID), POI_UPDATE_NOTE(
				EXTRA_STR_POIURI), POI_UPDATE_DISTANCES(EXTRA_SYNCING_LOCATION), POI_CLEAN_SYNC, POI_SYNC_IF_NEEDED;

		Set<String> requiredExtras;

		private Cmd(String... extras) {
			requiredExtras = new HashSet<String>();
			requiredExtras.addAll(Arrays.asList(extras));
		}

		/**
		 * @return null if no Cmd could be resolved from the intent
		 */
		public static Cmd resolve(Intent intent) {
			if (intent != null && intent.getAction() != null) {
				Cmd match = Cmd.valueOf(intent.getAction());
				for (String required : match.requiredExtras) {
					if (!intent.hasExtra(required)) {
						Log.w(TAG, "Unable to resolve intent to cmd, " + match
								+ ". Intent missing extra, " + required);
						return null;
					}
				}
				return match;
			}
			return null;
		}

		/**
		 * This service's intent filters will resolve any intents with the
		 * appropriate action names to this service.
		 */
		public Intent formIntent(Context context) {
			Intent i = new Intent(context, PoiService.class);
			i.setAction(this.name());
			return i;

		}
	}

}
