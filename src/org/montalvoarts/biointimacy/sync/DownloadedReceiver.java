package org.montalvoarts.biointimacy.sync;

import org.montalvoarts.biointimacy.Util;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Listen for {@link DownloadManager} to assert that a download is complete.
 * Match that with an existing POI in the database and set it with the location
 * of the thumbnail binary (as a URI)
 */
public class DownloadedReceiver extends BroadcastReceiver {

	private static final String TAG = Util.Tag.Bio_Sync.prefix("Rcvr");

	@Override
	public void onReceive(Context context, Intent intent) {
		if (!intent.getAction()
				.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
			return;
		}
		long downloadId = intent.getLongExtra(
				DownloadManager.EXTRA_DOWNLOAD_ID, -1);
		Log.d(TAG, "Received download complete signal, for downloadId "
				+ downloadId);
		if (downloadId <= 0) {
			Log.w(TAG, "Received an invalid download id. Ignoring, "
					+ downloadId);
			return;

		}
		DownloadManager downloader = (DownloadManager) context
				.getSystemService(Context.DOWNLOAD_SERVICE);

		// Get the new URI
		Uri thumbUri = downloader.getUriForDownloadedFile(downloadId);
		if (thumbUri == null) {
			Log.w(TAG, "Unable to download image associated with request id, "
					+ downloadId + ". Skipping this image");
			return;
		}

		Intent start = PoiService.Cmd.POI_UPDATE_THUMBURI.formIntent(context);
		start.putExtra(PoiService.EXTRA_LONG_DOWNLOADID, downloadId);
		start.putExtra(PoiService.EXTRA_STR_THUMBURI, thumbUri.toString());

		ComponentName name = context.startService(start);
		if (name == null) {
			Log.w(TAG,
					"For thumbUri " + thumbUri + ", unable to start service using intent, "
							+ start.getAction());
		} else {
			Log.d(TAG, "For downloadId " + downloadId
					+ ", requesting service, " + name.toShortString()
					+ ", to update POI's thumbNail URI to " + thumbUri);
		}

	}
}
