package org.montalvoarts.biointimacy.sync;

import java.text.DateFormat;
import java.util.Date;

import org.montalvoarts.biointimacy.Util;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;

import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteStore;
import com.evernote.edam.notestore.NoteStore.Client;
import com.evernote.edam.notestore.SyncChunk;
import com.evernote.edam.notestore.SyncChunkFilter;
import com.evernote.edam.notestore.SyncState;
import com.evernote.edam.type.Note;
import com.evernote.edam.userstore.Constants;
import com.evernote.edam.userstore.UserStore;
import com.evernote.thrift.TException;
import com.evernote.thrift.protocol.TBinaryProtocol;
import com.evernote.thrift.transport.THttpClient;

public class EvernoteClient {

	private static final String CLIENT_APP_NAME = "Montalvo/Biointimacy ";

	private static final String CLIENT_AUTH_TOKEN = "S=s310:U=301da7c:E=1473d9d2161:C=13fe5ebf565:P=1cd:A=en-devtoken:V=2:H=a84cf0753f1ddb403fb6fb96641d1034";
	// "S=s1:U=6a635:E=14624c0e6c7:C=13ecd0fbac9:P=1cd:A=en-devtoken:V=2:H=ccaff7aee581083099b5903e5ac2c15d";

	// Initial development is performed on our sandbox server. To use the
	// production
	// service, change "sandbox.evernote.com" to "www.evernote.com" and replace
	// your
	// developer token above with a token from
	// https://www.evernote.com/api/DeveloperToken.action
	private static final String CLIENT_EVERNOTE_HOST = "www.evernote.com";
	private static final String CLIENT_USERSTORE_URL = "https://"
			+ CLIENT_EVERNOTE_HOST + "/edam/user";

	// In a real application, you would change the User Agent to a string that
	// describes
	// your application, using the form company name/app name and version. Using
	// a unique
	// user agent string helps us provide you with better support.
	private static final String userAgent = CLIENT_APP_NAME
			+ Constants.EDAM_VERSION_MAJOR + "." + Constants.EDAM_VERSION_MINOR;

	private static final String TAG = Util.Tag.Bio_Sync.prefix("Client");

	private static final String CLIENT_PREF_FILE = "client.pref";
	private static final String CLIENT_PREF_LASTUSN = "PREF_LASTUSN";

	/**
	 * Last sync time in milliseconds since the epoch
	 */
	private static final String CLIENT_PREF_LASTSYNC = "PREF_LASTSYNC";

	private static final long MARGIN_MILLIS = 5 * 60 * 1000;

	private static final int CLIENT_MAX_SYNCENTRIES = 1000;

	private static final int CLIENT_THM_SIZE_PX = 600;

	private Context mContext;

	private int mLastUsn;
	private long mLastSync;

	private DownloadManager mDownloader;

	// Used to form the Thumbail Service endpoint
	private Uri mShardEndpoint;

	public EvernoteClient(Context context) {
		mContext = context;
		SharedPreferences pref = mContext.getSharedPreferences(
				CLIENT_PREF_FILE, Context.MODE_PRIVATE);
		mLastUsn = pref.getInt(CLIENT_PREF_LASTUSN, 0);
		mLastSync = pref.getLong(CLIENT_PREF_LASTSYNC, 0);
		mDownloader = (DownloadManager) mContext
				.getSystemService(Context.DOWNLOAD_SERVICE);
	}

	/**
	 * Completely clear all persisted state of this client. It will be as though
	 * client starting from a fresh install of the app.
	 */
	public void reset() {
		mLastSync = 0;
		mLastUsn = 0;
		mShardEndpoint = null;
		persistPref();
	}

	public long getLastSyncMillis() {
		return mLastSync;
	}
	/**
	 * Initializes the notestore object.
	 * 
	 * @return null if connection failed
	 */
	public Client connect() {
		try {
			// Set up the UserStore client and check that we can speak to the
			// server
			THttpClient userStoreTrans = new THttpClient(CLIENT_USERSTORE_URL);
			userStoreTrans.setCustomHeader("User-Agent", userAgent);
			TBinaryProtocol userStoreProt = new TBinaryProtocol(userStoreTrans);
			UserStore.Client userStore = new UserStore.Client(userStoreProt,
					userStoreProt);

			boolean versionOk = userStore.checkVersion(CLIENT_APP_NAME,
					com.evernote.edam.userstore.Constants.EDAM_VERSION_MAJOR,
					com.evernote.edam.userstore.Constants.EDAM_VERSION_MINOR);
			if (!versionOk) {
				throw new Exception(
						"Incompatible Evernote client protocol version, with this app having version: "
								+ userAgent);
			}

			// Get the URL used to interact with the contents of the user's
			// account
			String notestoreUrl = userStore.getNoteStoreUrl(CLIENT_AUTH_TOKEN);
			int delimIndex = notestoreUrl.lastIndexOf("/");
			mShardEndpoint = Uri.parse((String) notestoreUrl.subSequence(0,
					delimIndex));
			Log.d(TAG, "From noteStoreUrl " + notestoreUrl
					+ ", got shardEndpoint URL " + mShardEndpoint);

			// Set up the NoteStore client
			THttpClient noteStoreTrans = new THttpClient(notestoreUrl);
			noteStoreTrans.setCustomHeader("User-Agent", userAgent);
			TBinaryProtocol noteStoreProt = new TBinaryProtocol(noteStoreTrans);
			// Read-only notestore, no need to write
			return new NoteStore.Client(noteStoreProt);
		} catch (EDAMUserException ue) {
			Log.e(TAG,
					"Encountered exception that can be handled by user, with error code "
							+ ue.getErrorCode(), ue);
			return null;
		} catch (EDAMSystemException se) {
			Log.e(TAG,
					"Encountered exception that cannot be handled by user, with error code "
							+ se.getErrorCode(), se);
			return null;
		} catch (Exception e) {
			Log.e(TAG,
					"Unable to connect to note store. Got msg: "
							+ e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Checks the last USN according to the server. Server may communicate that
	 * a full sync is required. One side effect of this method is that the
	 * lastUSN is set for this client. The new value can be retrieved by
	 * {@link #getLastUsn()}
	 * 
	 * @return whether the client operation was successful or not
	 */
	public boolean checkAndSetLastUsn(Client client) {
		if (client == null) {
			return false;
		}
		try {
			SyncState syncState = client.getSyncState(CLIENT_AUTH_TOKEN);
			long currentTime = System.currentTimeMillis();
			Log.i(TAG, "Got current time " + syncState.getCurrentTime()
					+ ", fullSyncBefore " + syncState.getFullSyncBefore()
					+ ", uploaded " + syncState.getUploaded()
					+ ", updateCount " + syncState.getUpdateCount()
					+ ", phoneCurrentTime " + currentTime);

			DateFormat format = DateFormat.getDateTimeInstance();

			if (!fuzzyEquals(syncState.getCurrentTime(), currentTime)) {
				Log.w(TAG,
						"Server time, "
								+ format.format(new Date(syncState
										.getCurrentTime()))
								+ ", does not generally match the phone time, "
								+ format.format(new Date(currentTime))
								+ ". Unable to determine if a full sync required. lastUSN set to  "
								+ mLastUsn);
				return false;
			}
			Log.i(TAG,
					"Device clock and server clock generally matches. Device time: "
							+ format.format(new Date(currentTime))
							+ ", server clock is "
							+ format.format(new Date(syncState.getCurrentTime())));
			if (mLastSync < syncState.getFullSyncBefore()) {
				Log.i(TAG,
						"Setting lastUsn to 0. Last sync was at "
								+ format.format(new Date(mLastSync))
								+ " and server states that all clients should do a full sync if their last sync was before, "
								+ format.format(new Date(syncState
										.getFullSyncBefore())));
				mLastUsn = 0;
			} else {
				Log.i(TAG, "No need to do a full sync. lastUsn remains at "
						+ mLastUsn);
			}

			return true;
		} catch (EDAMUserException ue) {
			Log.e(TAG, "Unable to get sync state. " + ue.getErrorCode(), ue);
		} catch (EDAMSystemException se) {
			Log.e(TAG, "Unable to get sync state. " + se.getErrorCode(), se);
		} catch (TException e) {
			Log.e(TAG, "Can't get sync state", e);
		}
		return false;

	}

	public int getLastUsn() {
		return mLastUsn;
	}

	/**
	 * @return Sync chunk containing all changes since last sync.
	 * @return null if no chunk could be obtained or there is nothing to sync
	 */
	public SyncChunk getSyncChunk(Client client) {

		try {
			if (client == null) {
				throw new IllegalArgumentException("Invalid client");
			}
			SyncChunkFilter filter = new SyncChunkFilter();
			filter.setIncludeNotes(true);
			filter.setIncludeNoteAttributes(true);
			filter.setIncludeNoteResources(true);

			SyncChunk chunk = client.getFilteredSyncChunk(CLIENT_AUTH_TOKEN,
					mLastUsn, CLIENT_MAX_SYNCENTRIES, filter);

			if (!chunk.isSetChunkHighUSN() || chunk.getNotesSize() == 0) {
				Log.d(TAG,
						"No USN has been set or no notes present in chunk. This indicates that there are no objects in this chunk");
				return null;
			}
			// Save new sync state
			DateFormat format = DateFormat.getDateTimeInstance();
			mLastSync = chunk.getCurrentTime();
			mLastUsn = chunk.getChunkHighUSN();
			persistPref();

			Log.i(TAG, "Persisting lastUsn to be " + mLastUsn
					+ ", and lastSyncTime as " + format.format(mLastSync)
					+ ", numNotes(incl deleted) " + chunk.getNotesSize());

			return chunk;
		} catch (EDAMUserException ue) {
			Log.e(TAG, "Unable to sync. " + ue.getErrorCode(), ue);
		} catch (EDAMSystemException se) {
			Log.e(TAG, "Unable to sync. " + se.getErrorCode(), se);
		} catch (TException e) {
			Log.e(TAG, "Can't sync", e);
		}
		return null;
	}

	public Note getNote(Client client, String noteGuid) {
		try {
			Log.d(TAG, "Downloading note with guid, " + noteGuid);
			return client.getNote(CLIENT_AUTH_TOKEN, noteGuid, true, false,
					false, false);
		} catch (EDAMUserException ue) {
			Log.e(TAG, "Unable to get note. " + ue.getErrorCode(), ue);
		} catch (EDAMSystemException se) {
			Log.e(TAG, "Unable to get note. " + se.getErrorCode(), se);
		} catch (Exception e) {
			Log.e(TAG, "Can't get note", e);
		}
		return null;
	}

	public String shareNote(Client client, String noteGuid) {
		try {
			Log.d(TAG, "Sharing note with guid, " + noteGuid);
			return client.shareNote(CLIENT_AUTH_TOKEN, noteGuid);
		} catch (EDAMUserException ue) {
			Log.e(TAG, "Unable to share note. " + ue.getErrorCode(), ue);
		} catch (EDAMSystemException se) {
			Log.e(TAG, "Unable to share note. " + se.getErrorCode(), se);
		} catch (Exception e) {
			Log.e(TAG, "Can't share note", e);
		}
		return null;
	}

	/**
	 * @return identifier for this download request as provided by the
	 *         {@link DownloadManager}
	 * @return -1 if download request unsuccessful
	 */
	public long downloadResThumbnail(String resGuid, String extDir) {
		Uri uri = formResThumbUri(resGuid);
		if (uri == null) {
			throw new IllegalArgumentException(
					"Unable to get resource thumbnail URI for guid " + resGuid);
		}
		return downloadThumbnail(uri, resGuid, extDir);
	}

	/**
	 * @return identifier for this download request as provided by the
	 *         {@link DownloadManager}
	 * @return -1 if download request unsuccessful
	 */
	public long downloadNoteThumbnail(String noteGuid, String extDir) {
		Uri uri = formNoteThumbUri(noteGuid);
		if (uri == null) {
			throw new IllegalArgumentException(
					"Unable to get thumbnail URI for guid " + noteGuid);
		}
		return downloadThumbnail(uri, noteGuid, extDir);
	}

	public Uri formNotePublicUrl(String noteGuid, String noteKey) {
		if (mShardEndpoint == null) {
			return null;
		}
		Builder b = mShardEndpoint.buildUpon();
		b.appendPath("sh")
				.appendEncodedPath(noteGuid)
				.appendEncodedPath(noteKey);
		return b.build();
	}

	/**
	 * @return identifier for this download request as provided by the
	 *         {@link DownloadManager}
	 * @return -1 if download request unsuccessful
	 */
	private long downloadThumbnail(Uri remoteUri, String guid, String extDir) {
		try {
			if (remoteUri == null) {
				throw new IllegalArgumentException(
						"Unable to get thumbnail URI");
			}
			Log.d(TAG, "Enqueued request to download URI " + remoteUri);
			DownloadManager.Request req = new DownloadManager.Request(remoteUri);
			// Make it publicly available so that we can ask an ImageViewer to
			// view
			// these files
			 req.setDestinationInExternalPublicDir(extDir, guid);
			req.setNotificationVisibility(Request.VISIBILITY_HIDDEN);
			return mDownloader.enqueue(req);
		} catch (Exception e) {
			Log.w(TAG, "Unable to download thumbnail", e);
			return -1;
		}
	}

	private Uri formResThumbUri(String resGuid) {
		if (mShardEndpoint == null) {
			return null;
		}
		Builder b = mShardEndpoint.buildUpon();
		b.appendPath("thm")
				.appendPath("res")
				.appendEncodedPath(resGuid)
				.appendQueryParameter("size",
						String.valueOf(CLIENT_THM_SIZE_PX));
		return b.build();
	}

	private Uri formNoteThumbUri(String noteGuid) {
		if (mShardEndpoint == null) {
			return null;
		}
		Builder b = mShardEndpoint.buildUpon();
		b.appendPath("thm")
				.appendPath("note")
				.appendEncodedPath(noteGuid)
				.appendQueryParameter("size",
						String.valueOf(CLIENT_THM_SIZE_PX));
		return b.build();
	}

	private void persistPref() {
		SharedPreferences pref = mContext.getSharedPreferences(
				CLIENT_PREF_FILE, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putInt(CLIENT_PREF_LASTUSN, mLastUsn);
		editor.putLong(CLIENT_PREF_LASTSYNC, mLastSync);
		editor.commit();
	}

	private boolean fuzzyEquals(long time1, long time2) {
		return time1 < time2 + MARGIN_MILLIS && time1 > time2 - MARGIN_MILLIS;
	}

}
