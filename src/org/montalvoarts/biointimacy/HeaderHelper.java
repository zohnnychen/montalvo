package org.montalvoarts.biointimacy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HeaderHelper {

	protected static final String TAG = Util.Tag.Bio_Act.prefix("Header");
	private TextView mLink;
	private TextView mTitle;
	private Context mContext;
	private View mParent;
	private ImageView mImage;

	public HeaderHelper(Context context, View parent, TextView title, TextView link, ImageView image) {
		mContext = context;
		mTitle = title;
		mLink = link;
		mParent = parent;
		mImage = image;
		Util.setTypeface(mContext, mTitle, Util.TYPEFACE_TITLE);
		Util.setTypeface(mContext, mLink, Util.TYPEFACE_TITLE);
	}

	public void bind(String title, String link, Intent onClickIntent) {
		mParent.setVisibility(View.VISIBLE);
		mTitle.setText(title);
		mLink.setText(link);
		mImage.setVisibility(View.GONE);
		setClickable(mLink, onClickIntent);
	}


	public void bind(String title, String link, Bitmap image, Intent onClickIntent) {
		bind(title, link, onClickIntent);
		mImage.setVisibility(View.VISIBLE);
		mImage.setImageBitmap(image);
	}

	
	public void clear() {
		mParent.setVisibility(View.GONE);
		mTitle.setText("");
		mLink.setText("");
	}

	private void setClickable(TextView textView, final Intent intent) {
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		Spannable spans = (Spannable) textView.getText();
		ClickableSpan clickSpan = new ClickableSpan() {
			@Override
			public void onClick(View widget) {
				try {
					mContext.startActivity(intent);
					HeaderHelper.this.clear();
				} catch (Exception e) {
					Log.w(TAG, "Unable to launch intent " + intent, e);
				}
			}
		};
		spans.setSpan(clickSpan, 0, spans.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

	}
}
