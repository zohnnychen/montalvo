package org.montalvoarts.biointimacy;

import org.montalvoarts.biointimacy.sync.PoiService.Cmd;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

// Define a DialogFragment that displays the error dialog
public class GenericDialogFragment extends DialogFragment {
	protected static final String TAG = Util.Tag.Bio_Act.prefix("Dialog");
	// Global field to contain the error dialog
	private Dialog mDialog;

	// Default constructor. Sets the dialog field to null
	public GenericDialogFragment() {
		super();
		mDialog = null;
	}

	// Set the dialog to display
	public void setDialog(Dialog dialog) {
		mDialog = dialog;
	}

	// Return a Dialog to the DialogFragment.
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo);
		return mDialog;
	}

	public static Dialog buildSyncDialog(final Activity activity) {

		String[] options = new String[] {
				activity.getString(R.string.clean_sync),
				activity.getString(R.string.incremental_sync) };

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
				android.R.layout.simple_list_item_1, options);
		AlertDialog.Builder b = new AlertDialog.Builder(activity);
		b.setCancelable(true);

		TextView v = (TextView) activity.getLayoutInflater().inflate(
				android.R.layout.simple_list_item_1, null);
		v.setTextAppearance(activity,
				android.R.style.TextAppearance_DeviceDefault_Large);
		Util.setTypeface(activity, v, Util.TYPEFACE_TITLE);
		v.setText(R.string.sure_sync);
		b.setCustomTitle(v);

		b.setSingleChoiceItems(adapter, -1, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// index 0 is clean sync
				// index 1 is incremental sync
				switch (which) {
				case 0:
					activity.startService(Cmd.POI_CLEAN_SYNC
							.formIntent(activity));
					break;
				case 1:
					activity.startService(Cmd.POI_SYNC.formIntent(activity));
					break;
				}
				dialog.dismiss();
			}
		});
		return b.create();
	}

}