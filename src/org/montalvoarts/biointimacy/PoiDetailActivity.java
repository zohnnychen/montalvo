package org.montalvoarts.biointimacy;

import org.montalvoarts.biointimacy.Analytics.Event;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiRes;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusOneButton;

@SuppressWarnings("deprecation")
public class PoiDetailActivity extends Activity {

	private static final String TAG = Util.Tag.Bio_Act.prefix("Detail");
	private static final String SHARE_TYPE = "text/plain";
	private static final int REQ_CODE_PLUSONE = 100;
	private Poi mPoi;
	private PoiResAdapter mAdapter;
	private GoogleConnHelper mGoogleHelper;
	private PlusClient mPlusClient;
	private PlusOneButton mPlusOneButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poi_detail);

		Uri uri = getIntent().getData();
		Log.d(TAG, "Looking at uri " + uri);
		mPoi = PoiHelper.SINGLETON.create(this, uri);
		if (mPoi == null) {
			Log.w(TAG, "Unable to find POI matching uri " + uri);
			finish();
			return;
		}

		String title = mPoi.getString(Col.TITLE);
		if (!TextUtils.isEmpty(title)) {
			TextView titleV = (TextView) findViewById(R.id.detail_title);
			Util.setTypeface(this, titleV, Util.TYPEFACE_TITLE);
			titleV.setText(title);
		}

		String enml = mPoi.getString(Col.BODY);
		if (!TextUtils.isEmpty(enml)) {
			TextView bodyView = (TextView) findViewById(R.id.detail_body);
			Spanned bodyFromHtml = Html.fromHtml(enml);
			bodyView.setText(bodyFromHtml);
			// Respond to URL's in the HTML body
			bodyView.setMovementMethod(LinkMovementMethod.getInstance());
			Util.setTypeface(this, bodyView, Util.TYPEFACE_BODY);
		}

		Gallery gallery = (Gallery) findViewById(R.id.detail_gallery);
		mAdapter = new PoiResAdapter(this, mPoi);
		gallery.setAdapter(mAdapter);
		gallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				PoiRes resource = mAdapter.getItem(position);
				Intent i = resource.formIntent();
				if (i != null) {
					try {
						startActivity(i);
					} catch (Exception e) {
						Log.w(TAG,"Unable to view resource with intent " + i, e);
					}
				}
			}
		});

		// Track
		EasyTracker.getInstance().setContext(this);
		EasyTracker.getTracker().sendEvent(Event.VIEW_POI.category,
				Event.VIEW_POI.name(), title, null);

		mGoogleHelper = new GoogleConnHelper(this) {

			@Override
			public void onSuccessfullyConnected() {
				Log.d(TAG, "Connected Google plus client");
			}
		};
		PlusClient.Builder b = new PlusClient.Builder(this, mGoogleHelper,
				mGoogleHelper);
		b.clearScopes();
		mPlusClient = b.build();

		mPlusOneButton = (PlusOneButton) findViewById(R.id.detail_plusone);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		if (mPlusClient != null) {
			mPlusClient.connect();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
		if (mPlusClient != null) {
			mPlusClient.disconnect();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		String publicUriStr = mPoi.getString(PoiContract.Col.PUBLIC_URI);
		if (!TextUtils.isEmpty(publicUriStr)) {
			mPlusOneButton.initialize(mPlusClient, publicUriStr,
					REQ_CODE_PLUSONE);
		} else {
			mPlusOneButton.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_detail, menu);
		return true;
	}

	public void onClickShareIcon(View view) {
		String publicUriStr = mPoi.getString(PoiContract.Col.PUBLIC_URI);
		if (!TextUtils.isEmpty(publicUriStr)) {
			try {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType(SHARE_TYPE);
				intent.putExtra(Intent.EXTRA_TEXT, publicUriStr);
				intent.putExtra(Intent.EXTRA_SUBJECT,
						getString(R.string.share_subject));
				startActivity(Intent.createChooser(intent,
						getString(R.string.share_how)));

				// Track
				EasyTracker.getInstance().setContext(this);
				EasyTracker.getTracker()
						.sendEvent(Event.SHARE_POI.category,
								Event.SHARE_POI.name(),
								mPoi.getString(Col.TITLE), null);
			} catch (Exception e) {
				Log.w(TAG, "Unable to share note with uri " + publicUriStr, e);
			}
		}
	}

}
