package org.montalvoarts.biointimacy;

import java.lang.ref.WeakReference;
import java.util.List;

import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.data.PoiRes;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

@SuppressWarnings("deprecation")
public class PoiResAdapter extends ArrayAdapter<PoiRes> {

	private static final String TAG = Util.Tag.Bio_Act.prefix("Adapter");
	private int mWidth;
	private int mHeight;

	public PoiResAdapter(Context context, Poi poi) {
		super(context, -1);
		// Create mapping of Resource Hash2Guid
		int poiRowId = poi.getInteger(Col.ID);
		if (poiRowId < 0) {
			throw new IllegalArgumentException(
					"Argument POI is not associated with a database row id");
		}
		List<PoiRes> resources = PoiHelper.SINGLETON.listResources(context,
				poiRowId);
		for (PoiRes res : resources) {
			if (!res.isValid()) {
				Log.w(TAG, "Encountered invalid POI res " + res);
				continue;
			}
			if (res.isBlacklisted()) {
				Log.v(TAG, "Encountered blacklisted POI res " + res);
				continue;
			}
			add(res);
		}

		mWidth = (int) context.getResources().getDimension(
				R.dimen.gallery_image_width);
		mHeight = (int) context.getResources().getDimension(
				R.dimen.gallery_image_height);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = new ImageView(getContext());
		Gallery.LayoutParams params = new Gallery.LayoutParams(mWidth, mHeight);
		view.setImageResource(R.drawable.badge);
		view.setLayoutParams(params);

		view.setImageResource(R.drawable.badge);
		PoiRes res = getItem(position);
		Uri uri = res.getLocalUri();
		if (uri != null) {
			BitmapWorkerTask t = new BitmapWorkerTask(getContext(), view,
					mWidth, mHeight);
			t.execute(uri);
		}
		return view;
	}

	private static class BitmapWorkerTask extends AsyncTask<Uri, Void, Bitmap> {
		private final WeakReference<ImageView> imageViewReference;
		private Context mContext;
		private int mWidth;
		private int mHeight;

		public BitmapWorkerTask(Context context, ImageView imageView,
				int width, int height) {
			// Use a WeakReference to ensure the ImageView can be garbage
			// collected
			imageViewReference = new WeakReference<ImageView>(imageView);
			mContext = context;
			mWidth = width;
			mHeight = height;
		}

		// Decode image in background.
		@Override
		protected Bitmap doInBackground(Uri... params) {
			Uri data = params[0];
			return PoiHelper.SINGLETON.decodeSampledBitmap(mContext, data,
					mWidth, mHeight);
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
				if (imageView != null) {
					imageView.setImageBitmap(bitmap);
				}
			}
		}
	}
}
