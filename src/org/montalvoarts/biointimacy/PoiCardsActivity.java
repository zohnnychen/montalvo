package org.montalvoarts.biointimacy;

import org.montalvoarts.biointimacy.Analytics.Event;
import org.montalvoarts.biointimacy.data.Poi;
import org.montalvoarts.biointimacy.data.PoiContract;
import org.montalvoarts.biointimacy.data.PoiContract.Col;
import org.montalvoarts.biointimacy.data.PoiHelper;
import org.montalvoarts.biointimacy.sync.PoiService;
import org.montalvoarts.biointimacy.sync.PoiService.Cmd;
import org.montalvoarts.biointimacy.view.PoiCard;
import org.montalvoarts.biointimacy.view.ThumbCache;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fima.cardsui.views.CardUI;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class PoiCardsActivity extends Activity implements
		LoaderCallbacks<Cursor>, LocationListener {

	private static final int LOAD_ALL_POI = 100;

	private CardUI mCardView;
	private ThumbCache mCache;
	private BroadcastReceiver mReceiver;
	private ProgressBar mProgress;

	private LocationClient mLocationClient;

	private GoogleConnHelper mGoogleHelper;
	private HeaderHelper mHeaderHelper;
	private SharedPreferences mPref;

	private BroadcastReceiver mDistancesUpdatedReceiver;

	private static final String TAG = Util.Tag.Bio_Act.prefix("Cards");

	private static final long LOC_INTERVAL_MILLIS = 10000;

	private static final long LOC_FASTEST_INTERVAL_MILLIS = 5000;

	private static final String DIALOG_TAG_SYNC = "dialog_sync";

	private static final float MIN_ACCURACY_REQUIRED_METERS = 60.0f;

	@SuppressLint("DefaultLocale")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_poi_list);

		// Sync remotely and locally
		startService(PoiService.Cmd.POI_SYNC_IF_NEEDED.formIntent(this));		

		// Load results
		getLoaderManager().initLoader(LOAD_ALL_POI, null, this);
		mCardView = (CardUI) findViewById(R.id.list_cards);

		// Clear out cache
		mCache = new ThumbCache(this);
		mCache.initWithAllPoi(this);

		// Progress bar
		mProgress = (ProgressBar) findViewById(R.id.list_progress);
		initOnSyncFinishedReceiver();

		initOnDistancesReceiver();

		SpannableString s = Util
				.withCustomTypeface(this, getString(R.string.app_name)
						.toUpperCase(), Util.TYPEFACE_TITLE);

		// Update the action bar title with the TypefaceSpan instance
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(s);

		// Create Google Connection Helper
		mGoogleHelper = new GoogleConnHelper(this) {

			@Override
			public void onSuccessfullyConnected() {
				// Demand high accuracy, high power consumption location updates
				// Make sure to turn this off outside this activity
				LocationRequest locReq = LocationRequest.create();
				locReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
						.setInterval(LOC_INTERVAL_MILLIS)
						.setFastestInterval(LOC_FASTEST_INTERVAL_MILLIS);

				mLocationClient.requestLocationUpdates(locReq,
						PoiCardsActivity.this);
			}
		};

		mHeaderHelper = new HeaderHelper(this, findViewById(R.id.list_header),
				(TextView) findViewById(R.id.list_headertitle),
				(TextView) findViewById(R.id.list_headerlink),
				(ImageView) findViewById(R.id.list_headerimage));

		mLocationClient = new LocationClient(this, mGoogleHelper, mGoogleHelper);

		mPref = getSharedPreferences(Util.APP_PREF, MODE_PRIVATE);
	}

	private void initOnDistancesReceiver() {
		mDistancesUpdatedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(
						PoiService.ACTION_UPDATED_DISTANCES)) {
					Uri poiUri = intent
							.getParcelableExtra(PoiService.EXTRA_URI_CLOSESTPOI);
					if (poiUri == null) {
						Log.w(TAG, "Aborting. Unable to get URI from intent "
								+ intent);
						return;
					}
					Poi poi = PoiHelper.SINGLETON.create(PoiCardsActivity.this,
							poiUri);
					if (poi == null) {
						Log.w(TAG, "Unable to get poi at uri " + poiUri);
						return;
					}
					if (!TextUtils.isEmpty(poi.getString(Col.TITLE))
							&& poi.getFloat(Col.PROXIMITY_METERS) > 0) {
						Log.w(TAG, "Unable to obtain poi for uri " + poiUri);
						Bitmap thumb = mCache.getBitmapFromMemCache(poi
								.getString(Col.GUID));
						String title = getString(R.string.closest_point,
								poi.getString(Col.TITLE));
						String link = PoiHelper.SINGLETON
								.prettyEnglishDistance(
										poi.getFloat(Col.PROXIMITY_METERS),
										PoiCardsActivity.this);
						Intent onClickIntent = new Intent(Intent.ACTION_VIEW,
								ContentUris.withAppendedId(
										PoiContract.CONTENT_URI,
										poi.getInteger(Col.ID)));
						if (thumb != null) {
							mHeaderHelper.bind(title, link, thumb,
									onClickIntent);
						} else {
							mHeaderHelper.bind(title, link, onClickIntent);
						}
						Log.d(TAG,
								"Displaying POI " + title + " and uri "
										+ poiUri + " is closest at distance "
										+ poi.getFloat(Col.PROXIMITY_METERS));
					}
				}

			}
		};

		IntentFilter filter = new IntentFilter(
				PoiService.ACTION_UPDATED_DISTANCES);
		registerReceiver(mDistancesUpdatedReceiver, filter);
	}

	private void initOnSyncFinishedReceiver() {
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(PoiService.ACTION_SYNCING)
						&& intent.hasExtra(PoiService.EXTRA_SYNCING_ISACTIVE)) {
					boolean isSyncActive = intent.getBooleanExtra(
							PoiService.EXTRA_SYNCING_ISACTIVE, false);
					int visibility = isSyncActive ? View.VISIBLE
							: View.INVISIBLE;
					mProgress.setVisibility(visibility);
				}
			}
		};
		IntentFilter filter = new IntentFilter(PoiService.ACTION_SYNCING);
		registerReceiver(mReceiver, filter);
	}

	/*
	 * Called when the Activity is no longer visible at all. Stop updates and
	 * disconnect.
	 */
	@Override
	public void onStop() {

		// If the client is connected
		if (mLocationClient.isConnected()) {
			mLocationClient.removeLocationUpdates(this);
		}

		// After disconnect() is called, the client is considered "dead".
		mLocationClient.disconnect();
		EasyTracker.getInstance().activityStop(this);
		super.onStop();
	}

	/*
	 * Called when the Activity is restarted, even before it becomes visible.
	 */
	@Override
	public void onStart() {

		super.onStart();
		mHeaderHelper.clear();
		EasyTracker.getInstance().activityStart(this);
		/*
		 * Connect the client. Don't re-start any requests here; instead, wait
		 * for onResume()
		 */
		mLocationClient.connect();
		if (!Util.isGpsEnabled(this)) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			mHeaderHelper.bind(getString(R.string.enable_location_desc),
					getString(R.string.enable_location), intent);
		}
	}
	
	// Don't let users re-sync. May prevent the dreaded no-image bug
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.menu_cards, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		if (item.getItemId() == R.id.share) {
//			GenericDialogFragment newFragment = new GenericDialogFragment();
//			newFragment.setDialog(GenericDialogFragment.buildSyncDialog(this));
//			newFragment.show(getFragmentManager(), DIALOG_TAG_SYNC);
//		}
//		return super.onOptionsItemSelected(item);
//	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mCache.clear();
		if (mReceiver != null) {
			unregisterReceiver(mReceiver);
		}
		if (mDistancesUpdatedReceiver != null) {
			unregisterReceiver(mDistancesUpdatedReceiver);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (id == LOAD_ALL_POI) {
			return new CursorLoader(this, PoiContract.CONTENT_URI, Col.proj(),
					null, null, PoiContract.TITLE);
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// Add a few cards
		int numCards = 0;
		if (cursor != null && cursor.getCount() > 0) {
			mCardView.clearCards();
			while (cursor.moveToNext()) {
				Poi poi = PoiHelper.SINGLETON.create(this, cursor);
				if (poi != null && poi.isValid()) {
					PoiCard card = new PoiCard(this, poi, mCache);
					numCards++;
					mCardView.addCard(card);
				}
			}
			mCardView.refresh();
		}
		if (numCards > 0) {
			findViewById(R.id.list_badge).setVisibility(View.GONE);
		} else {
			findViewById(R.id.list_badge).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		mGoogleHelper.onActivityResult(requestCode, resultCode, intent);
	}

	@Override
	public void onLocationChanged(Location loc) {
		Log.d(TAG, "onLocationChanged " + loc);
		if (loc != null) {

			if (loc.getAccuracy() > MIN_ACCURACY_REQUIRED_METERS
					&& !mPref.getBoolean(Util.PREF_BOOL_ISSHOW_GOOGLEDIALOG,
							false)) {
				Log.d(TAG, "Accuracy is poor");
				Intent intent = new Intent(
						"com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS");
				mHeaderHelper.bind(
						getString(R.string.enable_google_location_desc),
						getString(R.string.enable_google_location), intent);
				mPref.edit()
						.putBoolean(Util.PREF_BOOL_ISSHOW_GOOGLEDIALOG, true)
						.commit();
			}

			updateLocation(loc);
		}
	}

	private void updateLocation(Location loc) {
		Intent i = Cmd.POI_UPDATE_DISTANCES.formIntent(this);
		i.putExtra(PoiService.EXTRA_SYNCING_LOCATION, loc);
		startService(i);
	}
}
